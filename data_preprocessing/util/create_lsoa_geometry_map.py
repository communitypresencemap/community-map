"""
Processes multiple GeoJSON input files and creates a mapping from LSOA codes to coordinates.
"""

import json

inputs = [('../data/URISK.json', 'geog_id'),
          ('../data/WIMD_2014.geojson', 'lsoa04cd'),
          ('../data/EIMD_2015.json', 'LSOA01CD'),
          ('../data/SIMD_2016.geojson', 'DataZone'),
          ('../data/NIMD_2017.geojson', 'SOA_CODE')]


def get_coordinates_for_lsoa_codes(coordinates_map: dict, input_path: str, code: str):
    with open(input_path, 'r') as f:
        data = json.load(f)

        features = data['features']

        for feat in features:
            lsoa_code = feat['properties'][code]
            geometry = feat['geometry']
            if lsoa_code not in coordinates_map:
                coordinates_map[lsoa_code] = geometry

    return coordinates_map


def create_lsoa_geometry_map():
    coordinates_map = {}

    for file, code in inputs:
        coordinates_map = get_coordinates_for_lsoa_codes(coordinates_map, file, code)

    with open('../data/lsoa_coordinates_map.json', 'w') as f:
        json.dump(coordinates_map, f)


if __name__ == '__main__':
    create_lsoa_geometry_map()