"""
Converts the synthetic dummy data representing internal BRC resources from
the initial JSON files to GeoJSON format.
"""
from conversion.convert_json_to_geojson import convert_json_to_geojson

volunteers_input_path = '../data/volunteers_dummy_data.json'
volunteers_output_path = '../data/volunteers.geojson'

staff_input_path = '../data/staff_dummy_data.json'
staff_output_path = '../data/staff.geojson'

if __name__ == '__main__':
    convert_json_to_geojson(volunteers_input_path, volunteers_output_path)
    convert_json_to_geojson(staff_input_path, staff_output_path)
