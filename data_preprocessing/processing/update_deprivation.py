"""
Converts the data representing deprivation from
the initial CSV file to GeoJSON format.
"""
import csv
import json

column_indices = {
    "LSOA": 0,
    "Name": 1,
    "RUC": 2,
    "IMD_decile": 3,
    "Income_decile": 4,
    "Employment_decile": 5,
    "Education_decile": 6,
    "Health_decile": 7,
    "Crime_decile": 8,
    "Housing_decile": 9,
    "Environment_decile": 10
}


def build_deprivation_geojson_from_csv(coordinates_map):
    deprivation = {'type': 'FeatureCollection', 'features': []}

    with open('../data/uk_imd_domains.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        id = 1
        for row in csv_reader:
            new_feature = {'type': 'Feature', 'id': id}

            lsoa = row[column_indices["LSOA"]]

            new_feature['properties'] = {
                "LSOA": lsoa,
                "Name": row[column_indices["Name"]],
                "RUC": row[column_indices["RUC"]],
                "IMD Decile": row[column_indices["IMD_decile"]],
                "Income Decile": row[column_indices["Income_decile"]],
                "Employment Decile": row[column_indices["Employment_decile"]],
                "Education Decile": row[column_indices["Education_decile"]],
                "Health Decile": row[column_indices["Health_decile"]],
                "Crime Decile": row[column_indices["Crime_decile"]],
                "Housing Decile": row[column_indices["Housing_decile"]],
                "Environment Decile": row[column_indices["Environment_decile"]]
            }

            if lsoa in coordinates_map:
                new_feature['geometry'] = coordinates_map[lsoa]
            else:
                new_feature['geometry'] = {}

            deprivation['features'].append(new_feature)
            id += 1

    return deprivation


def create_deprivation_geojson():
    with open('../data/lsoa_coordinates_map.json', 'r') as f:
        coordinates_map = json.load(f)

    deprivation = build_deprivation_geojson_from_csv(coordinates_map)

    with open('../data/deprivation.geojson', 'w') as f:
        json.dump(deprivation, f)


if __name__ == '__main__':
    create_deprivation_geojson()
