"""
For the keys representing index domains we use, enriches  risk data. If the
respective domain is missing from a feature's properties, it sets it to
-1. If the value exists, but is 0, it sets it to 1 instead.
"""

import json

used_domain_keys = [
    "fires_q",
    "floods_q",
    "lonely_q",
    "health_q",
    "imd_q",
    "alone_q"
]


def create_risks_geojson():
    with open('../data/URISK.json', 'r') as f:
        risk_data = json.load(f)

    for feature in risk_data['features']:
        for domain in used_domain_keys:
            properties = feature['properties']
            if domain not in properties:
                properties[domain] = -1
            if properties[domain] == 0:
                properties[domain] = 1

    with open('../data/URISK.geojson', 'w') as f:
        json.dump(risk_data, f)


if __name__ == '__main__':
    create_risks_geojson()
