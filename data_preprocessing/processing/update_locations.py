"""
Converts the synthetic dummy data representing internal BRC resources from
the initial JSON files to GeoJSON format.
"""
from conversion.convert_geometry_collection_to_feature_collection import convert_geometry_collection_to_feature_collection

lrf_input_path = '../../data/LRFs_and_EPGs_and_LRPs.json'
lrf_output_path = '../../data/LRF.geojson'

ccg_input_path = '../../data/UK_health_boundaries.json'
ccg_output_path = '../../data/CCG.geojson'

if __name__ == '__main__':
    convert_geometry_collection_to_feature_collection(lrf_input_path, lrf_output_path)
    convert_geometry_collection_to_feature_collection(ccg_input_path, ccg_output_path)
