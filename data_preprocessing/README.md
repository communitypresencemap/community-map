# Community Presence Map - Data Preprocessing Scripts

## Overview

This page describes the steps that are needed to create tile sets starting from the raw data files.

## Requirements

* Python 3
* Tippecanoe
* gdal
* topojson-client

### Installing/running on Windows
Install [Docker Desktop](https://www.docker.com/products/docker-desktop). We'll need to modify an [existing Docker image](https://hub.docker.com/r/jskeates/tippecanoe) to have the latest version of Tippecanoe before we can run it. To do so, run this command:

```
docker build -t tippecanoe --build-arg TIPPECANOE_RELEASE=1.34.3 https://github.com/jskeates/docker-tippecanoe.git
```

As Tippecanoe gets updated, you may need to change the `TIPPECANOE_RELEASE` argument to a [later version](https://github.com/mapbox/tippecanoe/releases).

## Raw Data Formats

Raw data in GeoJSON format can be converted into tile sets directly. There is some preprocessing required to transform
raw data in CSV, JSON or TopoJSON format into GeoJSON.

The geography elements in the raw data may have different forms, including GeoJSON-compatible point or
polygon coordinates, but also postcodes and LSOA geography codes. When the geography information is not in a form
compatible with GeoJSON, it has to be transformed so that it is compatible.

#### LSOA Codes
The map ```lsoa_coordinates_map.json```, which was created using the script ```create_lsoa_geometry_map.py```, may be used to directly transform LSOA codes into GeoJSON geometry objects.

#### TopoJSON
TopoJSON is an extension of GeoJSON, which uses topologies. To convert a TopoJSON dataset to GeoJSON, use the following
command:
```shell script
topo2geo object_name=output.json < input.json
```
The ```object_name``` is the name of the object in the input TopoJSON.

## Data Cleaning

In some occasions, we have had to perform data cleaning on the GeoJSON files, either due to missing data or because of
bad data, more specifically non UTF-8 characters in the data.

If there is missing geographical data in the GeoJSON, the command to generate the tile set will fail. In these cases,
we have either removed the elements with an incomplete `geography` object, or we have filled in the geographical data
ourselves by searching for it online.

In the case of non UTF-8 characters, we have replaced those characters with UTF-8 compatible ones.

## Creating Vector Tiles

The raw data needs to be transformed into valid GeoJSON. The GeoJSON files are used to generate the tile sets used by Mapbox.

The required format of the GeoJSON files is:
```shell script
{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [125.6, 10.1]
      },
      "properties": {
        "name": "Dinagat Islands"
      }
    }
  ]
}
```

### Running on Windows
Start the Docker container with Tippecanoe installed:

```
docker run -it -v $HOME/:/home/tippecanoe tippecanoe
```

Navigate to the `community-map/data` folder and run the command below.

### Generating the vector tiles
We generate the vector tile sets from GeoJSON using Tippecanoe. The command we run is:

```
tippecanoe -z14 -Z0 -b0 -r1 -pk -pf -o out.mbtiles in.geojson
```

or in more detail:

```
tippecanoe \
    --maximum-zoom=14 \
    --minimum-zoom=0 \
    --buffer=0 \
    --drop-rate=1 \
    --no-tile-size-limit \
    --no-feature-limit \
    --output=out.mbtiles \
    in.geojson
```

Make sure that the relevant entry in `frontend/src/Core/Config/config.json` is the same as the filename you choose for `out.mbtiles`. For example, the entries:

```
"sources": [
          {
            "label": "Middle Layer Super Output Areas (MSOA)",
            "value": "risks-msoa.json"
          },
          {
            "label": "Local Authorities (LAD)",
            "value": "risks-lad.json"
          }
        ]
```

## Running the Scripts
To run a script, make sure to have the required input data ready in the `data` directory. To make sure relative module
imports will work, ensure that the root script directory is included in the `PYTHONPATH`. This is done by default when
you create a PyCharm run-configuration, or can be done when invoking Python on the terminal:

```shell script
cd data_processing
PYTHONPATH=. python3 ./processing/add_csv_properties_to_geojson.py
```

## Projections
The projection we use in our datasets is EPSG:4326 (WGS84). If the GeoJSON data is not that projection, the command for
creating the tile set will fail. Therefore, it is necessary to convert the features in the GeoJSON to the right projection.
This is achieved using the following ```ogr2ogr``` command (from ```gdal```).
```shell script
ogr2ogr -s_srs EPSG:27700 -t_srs EPSG:4326 -f GeoJSON output.geojson input.geojson
```
In this case, EPSG:27700 is the source projection (SRS), and EPSG:4326 is the desired projection in which to transform to.

Note: EPSG:27700 is the British National Grid system.

## Existing Files
Category | GeoJSON | Vector Tile Set
--- | --- | ---
Deprivation | deprivation.geojson | deprivation.mbtiles
Risks | msoa-all.geojson | risks.mbtiles
Destitution | destitution.geojson | destitution.mbtiles
Demographics | demographics.geojson | demographics.mbtiles
Staff | staff.geojson | staff.mbtiles
Volunteers | volunteers.geojson | volunteers.mbtiles
Hospitals | hospitals.geojson | hospitals.mbtiles
Shops | shops.geojson | shops.mbtiles
Local Authority Outline | Local_Authority_Districts_April_2019_Boundaries_UK_BUC.geojson | locations_local_authority.mbtiles
CCG, Health Board and Trust Outline | locations_ccg.geojson | locations_ccg.mbtiles
Sustainability and Transformation Partnership Outline | Sustainability_and_Transformation_Partnerships_April_2019_EN_BUC.geojson | locations_sustainability_and_transformation.mbtiles
Local Resilience Forum Outline | locations_lrf.geojson | locations_lrf.mbtiles
County Outline | Counties_and_Unitary_Authorities_April_2019_Boundaries_UK_BUC.geojson | locations_county.mbtiles
Major Towns and Cities Outline | Major_Towns_and_Cities_December_2015_Boundaries.geojson | locations_towns_and_cities.mbtiles
Constituency Outline | Westminster_Parliamentary_Constituencies_December_2018_UK_BUC.geojson | locations_constituency.mbtiles
IL/CR Outline | locations_ilcr.geojson | locations_ilcr.mbtiles
RS/RFL Outline | locations_rsrfl.geojson | locations_rsrfl.mbtiles

## Further Readings

* GeoJSON: https://geojson.org/
* GeoJSON format: https://tools.ietf.org/html/rfc7946
* Tippecanoe and how to install it: https://github.com/mapbox/tippecanoe
* TopoJSON: https://github.com/topojson/topojson
* gdal and ogr2ogr: https://gdal.org/programs/ogr2ogr.html
* EPSG projections (UK): https://epsg.io/?q=United%20Kingdom%20kind:PROJCRS&page=1
