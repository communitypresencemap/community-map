# Community Presence Map
[![pipeline status](https://gitlab.com/communitypresencemap/community-map/badges/master/pipeline.svg)](https://gitlab.com/communitypresencemap/community-map/commits/master)

Community Presence Map is a web application used internally within [British Red Cross](https://www.redcross.org.uk/) built with a [React](https://facebook.github.io/react/) frontend.

> **NOTE:** This repo is public so make sure you don't commit any sensitive data/credentials!

## Mission Statement

**We folks are enabling Red Cross to make quantified decisions with a smooth mapping web application.**  

## Principles

* We think long term and maintainability
* Just ship it
* Ad-hoc pairing
* Build, Test, Learn, Repeat

## System Architecture

![Architecture](diagrams/system_diagram_v2.png)

(1) GitLab - Repository, Issue tracker, Wiki, CI/CD, Container registry  
(2) Azure Cloud - One ressource group for hosting the web application  
(3) WebApp - ressource in charge of providing hosting for the docker containers  
(4) Storage blob - space to store data for do be displayed in the application  
(5) (Future) Access control - ressource that enable user authentication via OneLogin or Active Directory   
(6) (Future) Azure function that provide a way to ingest new data for the application  

## Components

| Directory |  Description |
| --- | --- |
| [`frontend`](frontend) | Community Presence Map frontend written in TypeScript with React |
| [`infrastructure`](infrastructure) | Build the system's infrastructure using Terraform |
| [`data preparation`](data_preparation) | How to prepare data for processing |
| [`data processing`](data_preprocessing) | How to process data |

 > More detailed documentation for each component is available in their `README.md`

## Running the application

### Download required data

In order for the `tileserver` to run, the required mbtiles data needs to be downloaded (currently stored in `Teams > General > Files > data_sources > mbtiles`) and placed in the `data` directory.

Refer to `data/config.json` for an overview of required files. If one of the files is missing, `tileserver` will crash upon starting the service.

### Install frontend dependencies
After checking out the code:

```console
cd frontend
yarn install
cd ..
```

### Run frontend

```console
cd frontend
yarn start
```

Note: This will only start the development server for the frontend, not the `tileserver`. To facilitate development, it is therefore encouraged to use Docker Compose to start up all services.


### Run with Docker Compose

First, make sure you are logged into the container registry: Run the following 
command and use your GitLab credentials.

```console
docker login registry.gitlab.com/communitypresencemap/community-map
```

To run the development containers locally based off your current files with hot reloading use

```console
docker-compose up
```

To run based on the latest versions of the containers from the [container registry](https://gitlab.com/communitypresencemap/community-map/container_registry) use

```console
docker-compose -f docker-compose.registry.yml up
```

And when you're done, don't forget to run

```console
docker-compose down
```

## Coding Style

* Trunk based development
* Monorepo for whole project

## CI/CD

We are using GitLab's in-built CI/CD. Currently, this is not optimised 
for monorepo projects, thus there is a single [`.gitlab-ci.yml`](.gitlab-ci.yml) config file in the root of the repo.

Hopefully, in an upcoming release of GitLab we will see much richer support for a monorepo CI/CD. 
Follow [this issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972) for updates. The release is currently scheduled for late 2019.

### Overview

The pipeline has 3 stages: build, test and deploy.

#### Build

This checks that the frontend can build properly. The frontend
pipeline runs on the [`node` image](https://hub.docker.com/_/node).

#### Test

This runs the unit tests on the frontend.

#### Deploy

This job runs on the [`docker` image](https://hub.docker.com/_/docker).
It builds the frontend Docker image and pushes it to the [container registry](https://gitlab.com/communitypresencemap/community-map/container_registry).

### Environment Variables

We use some environment variables that are available to you out-of-the-box by GitLab,

```
$CI_REGISTRY_USER
$CI_REGISTRY_PASSWORD
$CI_REGISTRY
```

And we created the following,

| Varaible | Description |
| --- | --- |
| $AZ_WEBHOOK_URL | Once your Azure Web App is up an running you can get a webhook URL from the Download Pro |
| $REACT_APP_MAPBOX_ACCESS_TOKEN | An API access token is required to use Mapbox GL. See https://www.mapbox.com/api-documentation/#access-tokens-and-token-scopes | 

![Get a webhook  URL from Azure Web App](diagrams/az-web-app-publish-profile.png)

## Capturing Knowledge

There is a file [`LOGS.md`](LOGS.md) for capturing the outcomes of tests and spikes.

## Issues

We are using GitLab's in-built Issues tracking system. This can be found [here](https://gitlab.com/communitypresencemap/community-map/issues).

You can also view the issues on a [board](https://gitlab.com/communitypresencemap/community-map/-/boards).

## Contributing

The pipeline for forked projects will always fail in the deploy stage as you do not have access to the docker registry,
but merge requests can be accepted as long as the two prior stages succeed.

![Example Forked Pipeline](diagrams/Example-Forked-Pipeline.png)

In addition, please ensure that you only create a pull request for merging into the master branch of the community presence map project.

## Authors

* [Alasdair Hall](mailto:alasdair.hall@zuhlke.com)
* [Daphne Michalsen Tsallis](mailto:daphne.tsalli@zuhlke.com)
* [Niels Boecker](mailto:niels.boecker@zuhlke.com)
* [Raul Rodriguez](mailto:raul.rodriguez@zuhlke.com)
* [Ryan Collins](mailto:ryan.collins@zuhlke.com)

## License

This project is under MIT license [LICENSE](LICENSE)
