# Logs
This file contains tests, spikes and other usefull information captured whilst building the app.

## Performance Measures
### 16/8/19

We ran the current app serving flat files from kotlin backend to React front end. Everything runs locally, used mpb2k15
--> 10.42s to load 50MB `.geojson`

## Spikes
### [Investigate Leaflet and Google Maps](#2)

| Library | Tiles | GeoJSON |
| ------ | ------ | ------ |
| Leaflet | ✅ | ✅ |
| Google Maps | ✅ | ✅ |

Basically, we could use any of the 3: Mapbox, Leaflet and Google Maps.

My preference is for Mapbox because it seems to perform better than Leaflet and has good integration with  Mapbox studio styles and vector tiles which could be great for performance.

### [Test Tippecanoe and figure good practices for converting GEOJSON files to MBTILES tilesets](#9)

- Tippecanoe is quite straightforward to use
- Can then start a Tileserver from the directory you created the tiles in
- Can then query the Tileserver and display the data in a Mapbox map
- Load time of England deprivation was <1 second (the original GeoJSON file had already been processed to reduce its size)

### [test mapbox-gl-native and various wrappers (node-mapbox-gl-native, tileserver-gl, mbtileserver)](#8)

[tileserver](http://tileserver.org/) v [mbtileserver](https://github.com/consbio/mbtileserver)
- Install and run tileserver quickly and easily using Docker.
- Couldn't get mbtileserver to install properly using Go.
- Looks like they do the same sort of thing, but my preference would be for tileserver as it is much easier to get up and running.

For more detail on using Tileserver, my findings are in [this article](https://gitlab.com/communitypresencemap/community-map/wikis/Using-Tileserver-GL) on the wiki.


I don't think [node-mapbox-gl-native](https://github.com/mapbox/mapbox-gl-native/tree/master/platform/node) is relevant to this project. We can do everything using the Mapbox GL JS frontend library and add vector tile layers which are served from our tile server.

### [PostgreSQL + PostGIS basic test + instructions](#11)
This is documented in [this article](https://gitlab.com/communitypresencemap/community-map/wikis/Creating-a-custom-tile-server-for-English-deprivation-data) on the wiki.

### [Deploying from Gitlab to Azure](#19)
It looks like using Azure's App Service will be an appropriate solution for us. [Here](https://gitlab.com/communitypresencemap/community-map/wikis/Azure-App-Services-Web-App) is an article on how to set it up.

We also explored using Kubernetes on Azure, but determined it's probably a little over the top for our current needs. To find out more, read [here](https://gitlab.com/communitypresencemap/community-map/wikis/Azure-Kubernetes).
This is documented in [this article](https://gitlab.com/communitypresencemap/community-map/wikis/Creating-a-custom-tile-server-for-English-deprivation-data) on the wiki.

### [Searching for places](#40)
An article [here](https://gitlab.com/communitypresencemap/community-map/wikis/Searching-for-locations) exploring some of the options with the preference being for Open Street Map's [Nominatim](https://nominatim.openstreetmap.org).