namespace :project do
  desc "Run terraform plan for the project resources"
  task :plan do
    include Schott::Terraform
    validate_tf_project($configuration.project_name, $configuration)
    terraform_config = File.join($configuration.base, $configuration.azure_project)
    output_state = File.expand_path("out/project/gen/#{$configuration.project_name}.plan")
    mkdir_p(File.dirname(output_state))

    run_terraform("plan", " -out=#{output_state}", terraform_config)
  end
  desc "Apply the terraform configuration for the project resources"
  task :apply do
    include Schott::Terraform
    validate_tf_project($configuration.project_name, $configuration)
    terraform_config = File.join($configuration.base, $configuration.azure_project)

    run_terraform("apply", "-auto-approve", terraform_config)
  end
  task :show do
    include Schott::Terraform
    validate_tf_project($configuration.project_name, $configuration)
    terraform_config = File.join($configuration.base, $configuration.azure_project)
    Dir.chdir(terraform_config) do
      sh("terraform show")
    end
  end
  task :import do
    include Schott::Terraform
    validate_tf_project($configuration.project_name, $configuration)

    terraform_config = File.join($configuration.base, $configuration.azure_project)
    tf_address = ENV.fetch("TF_ADDRESS", "")
    res_id = ENV.fetch("TF_ID", "")

    raise "Missing parrameters (TF_ADDRESS or TF_ID empty)" if tf_address.empty? || res_id.empty?

    run_terraform("import", "#{tf_address} #{res_id}", terraform_config)
  end
  desc "Loads the system (containers and files) into the infrastructure"
  task :load do
    include Schott::AzureCLI
    update_web_app_settings($configuration)
    add_web_app_storage_account($configuration)
    update_web_app_and_deploy_containers($configuration)
  end
  desc "Destroy the current terraform state"
  task :destroy do
    include Schott::Terraform
    terraform_config = File.join($configuration.base, $configuration.azure_project)
    output_state = File.expand_path("out/project/gen/#{$configuration.project_name}.tfstate")
    input_state = File.expand_path("out/project/#{$configuration.project_name}.tfstate")
    run_terraform("destroy", "-state=#{input_state} -state-out=#{output_state}", terraform_config)
  end
end
