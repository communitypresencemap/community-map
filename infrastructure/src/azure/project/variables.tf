variable "project" {
  type        = string
  description = "The name of the project."
}

variable "subscription" {
  type = string
  description = " The Azure subscription ID used by the project"
}

variable "owner" {
  type        = string
  description = "The name of the project."
}

variable "storage_account_name" {
  type        = string
  description = "The name of the project's storage."
}

variable "foundation_resource_group_name" {
  type        = string
  description = "Only used in the foundation infrastructure."
}

variable "app_storage_account_name" {
  type        = string
  description = "The name of the project's web app storage."
}