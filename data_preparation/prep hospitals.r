library(tidyverse)

source("init.r")

#############################################################################################
## Hospitals (mark ones we have IL contracts in)
##
# hospitals data produced during creation of NHS map (see `copy data.r` in that project)
hospitals = read_csv(file.path(data.dir, "NHS/UK hospitals.csv"))

# load IL contracts
il_contracts = read_csv(file.path(data.dir, "Services/IL", "Contract Details.csv"))

# lookup hospitals from il_contracts
il_hosp = il_contracts %>% 
  select(`Main Hospital`) %>% 
  distinct() %>% 
  left_join(hospitals %>% select(Name) %>% mutate(Yes = "X"),
            by = c("Main Hospital" = "Name"))

# manually collated list from any rows in `il_hosp` where the `Yes` column is NA
other_hospitals = c("Felixstowe Community Hospital", "Bluebird Lodge", "Royal Free Hospital",
                    "King's College Hospital", "Chelsea and Westminster Hospital", "West Middlesex University Hospital",
                    "St Mary's Hospital", "Charing Cross Hospital", "Macclesfield District General Hospital",
                    "Bedford Hospital North Wing", "Bedford Hospital South Wing", "Basingstoke And North Hampshire Hospital",
                    "Ystradgynlais Community Hospital")

# mark which hospitals contain BRC services
hospitals = hospitals %>%
  left_join(il_contracts %>% select(`Main Hospital`) %>% mutate(`BRC services?` = 1),
            by = c("Name" = "Main Hospital")) %>% 
  
  # some we'll have to mark manually
  mutate(`BRC services?` = case_when(
    Name %in% other_hospitals ~ 1,
    is.na(`BRC services?`) ~ 0,
    TRUE ~ `BRC services?`
  ))

# get rid of any duplicates
hospitals = hospitals %>% distinct()

# convert to geojson
hospitals_geojson = geojson_json(hospitals)
geojson_write(hospitals_geojson, file = "../web app/data/hospitals.geojson")
