##
## prepare Local Authority boundaries with data about risks for emergencies/disasters, displacement, and health/social care
##
library(tidyverse)
library(geojsonio)
library(jsonlite)

# for map layers
library(rgdal)
library(rmapshaper)
library(spdplyr)

source("init.r")


####################################################################################################
## Make GeoJSON of Local Authorities with overlapping risks
##

##
## Load and process boundaries
##
# load Local Authority Districts for UK
lad_bounds = readOGR(dsn = file.path(input.data.dir, "Boundaries", "LADs"), 
                     layer = "LADs",
                     verbose = F)

# convert to web map CRS and simplify boundaries
map_proj = CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")

lad_bounds = spTransform(lad_bounds, map_proj)

lad_bounds = ms_simplify(lad_bounds)

##
## load risk data
##
risk_eng = read_csv(file.path(input.data.dir, "Risk maps", "England - LAD - risks.csv"))
risk_wal = read_csv(file.path(input.data.dir, "Risk maps", "Wales - LAD - risks.csv"))
risk_sco = read_csv(file.path(input.data.dir, "Risk maps", "Scotland - LAD - risks.csv"))
risk_ni  = read_csv(file.path(input.data.dir, "Risk maps", "NI - LAD - risks.csv"))

risk_eng = risk_eng %>% 
  select(lad17cd = LAD17CD, CombinedRisk,
         fires_q = worst_fires_q, floods_q = worst_floods_q, lonely_q = worst_lonely_q, health_q = worst_health_q, hle_birth_q, displacement_q, migrant_dest_q, digital_q, alone_q = worst_alone_q, destitution_q, imd_q = worst_imd_q,
         median_fires, median_floods, median_lonely, median_health, HLE_birth, n_asylum = Sec95, destitution_migrant, median_alone, destitution_all, median_imd)

risk_wal = risk_wal %>% 
  select(lad17cd = LAD17CD, CombinedRisk, 
         floods_q = worst_floods_q, lonely_q = worst_lonely_q, health_q = worst_health_q, hle_birth_q, displacement_q, migrant_dest_q, digital_q, alone_q = worst_alone_q, destitution_q, imd_q = worst_imd_q,
         median_floods, median_lonely, median_health, HLE_birth, n_asylum = Sec95, destitution_migrant, median_alone, destitution_all, median_imd)

risk_sco = risk_sco %>% 
  select(lad17cd = LAD17CD, CombinedRisk, 
         fires_q = worst_fires_q, lonely_q = worst_lonely_q, health_q = worst_health_q, hle_birth_q, displacement_q, migrant_dest_q, digital_q, alone_q = worst_alone_q, destitution_q, imd_q = worst_imd_q,
         median_fires, median_lonely, median_health, HLE_birth, n_asylum = Sec95, destitution_migrant, median_alone, destitution_all, median_imd)

risk_ni = risk_ni %>% 
  select(lad17cd = LAD18CD, CombinedRisk,
         floods_q = worst_floods_q, lonely_q = worst_lonely_q, health_q = worst_health_q, hle_birth_q, displacement_q, digital_q, alone_q = worst_alone_q, imd_q = worst_imd_q,
         median_floods, median_lonely, median_health, HLE_birth, n_asylum = Sec95, median_alone, median_imd)

risk_uk = bind_rows(risk_eng, risk_wal, risk_sco, risk_ni)


##
## make version with all LADs (regardless of their risk levels)
##
# merge into LAD boundaries, remove LADs without overlapping risks, and remove other unnecessary variables
lad_all = lad_bounds %>% 
  select(lad17cd, lad17nm) %>% 
  left_join(risk_uk, by = "lad17cd") %>% 
  dplyr::select(-CombinedRisk) %>% 
  select(-lad17cd, -lad17nm, everything())  # move LAD code and name to the end

# convert to geojson
lad_json = geojson_json(lad_all)

# save as geoJSON
geojson_write(lad_json, file = "../data/risks-lad.geojson")


###############################################################################
## Old code - keeping for posterity
##

##
## make version showing only LADs with overlapping risks
##
# keep only LADs which score 5 in more than one risk category
# risk_uk_overlap = risk_uk %>% 
#   mutate(Overlapping = ifelse(fires_q == 5, 1, 0) + ifelse(floods_q == 5, 1, 0) + ifelse(lonely_q == 5, 1, 0) + 
#            ifelse(health_q == 5, 1, 0) + ifelse(hle_birth_q == 5, 1, 0) + ifelse(displacement_q == 5, 1, 0) + ifelse(migrant_dest_q == 5, 1, 0) + ifelse(digital_q == 5, 1, 0)) %>% 
#   filter(Overlapping > 1)
# 
# # merge into LAD boundaries, remove LADs without overlapping risks, and remove other unnecessary variables
# lad_overlap = lad_bounds %>% 
#   left_join(risk_uk_overlap, by = "lad17cd") %>% 
#   filter(!is.na(CombinedRisk)) %>% 
#   dplyr::select(lad17cd, lad17nm, fires_q, floods_q, lonely_q, health_q, hle_birth_q, displacement_q, migrant_dest_q, digital_q, CombinedRisk)
# 
# # convert to geojson
# overlap_json = geojson_json(lad_overlap)
# 
# # save as geoJSON
# geojson_write(overlap_json, file = "../web app/data/lad-overlap.geojson")
# 
# # need to insert `var X = ` before the geoJSON data - can't do this and still write with geojson_write() so just reload the file as text and prepend
# overlap.js = read_lines("../web app/data/lad-overlap.geojson")    # load file as raw text
# overlap.js = paste0("var riskOverlap = ", overlap.js)          # prepend JS variable declaration
# write_lines(overlap.js, path = "../web app/data/lad-overlap.js")  # save new file
# 
# 
# ##
# ## make version with all LADs with at least one high risk
# ##
# # merge into LAD boundaries, remove LADs without overlapping risks, and remove other unnecessary variables
# lad_all = lad_bounds %>% 
#   left_join(risk_uk, by = "lad17cd") %>% 
#   filter(CombinedRisk != "") %>% 
#   dplyr::select(lad17cd, lad17nm, fires_q, floods_q, lonely_q, health_q, hle_birth_q, displacement_q, migrant_dest_q, digital_q, CombinedRisk)
# 
# # convert to geojson
# lad_json = geojson_json(lad_all)
# 
# # save as geoJSON
# geojson_write(lad_json, file = "../web app/data/lad-risk.geojson")
# 
# # need to insert `var X = ` before the geoJSON data - can't do this and still write with geojson_write() so just reload the file as text and prepend
# lad.js = read_lines("../web app/data/lad-risk.geojson")    # load file as raw text
# lad.js = paste0("var riskLADs = ", lad.js)                 # prepend JS variable declaration
# write_lines(lad.js, path = "../web app/data/lad-risk.js")  # save new file
