##
## Make GeoJSONs for Middle Layer Super Output Areas (and devolved nations' equivalents)
##
library(tidyverse)
library(geojsonio)
library(jsonlite)

# for map layers
library(rgdal)
library(rmapshaper)
library(spdplyr)

data.dir = "../../../../../Data"

##
## load and process boundaries
##
# Middle Layer Super Output Areas (MSOAs) for England
msoa_eng = readOGR(dsn = file.path(data.dir, "Boundaries/England and Wales"),
                   layer = "MSOA_England",
                   verbose = F)

# Middle Layer Super Output Areas (MSOAs) for England and Wales
msoa_ew = readOGR(dsn = file.path(data.dir, "Boundaries/England and Wales"),
                  layer = "Middle_Layer_Super_Output_Areas_December_2011_Super_Generalised_Clipped_Boundaries_in_England_and_Wales",
                  verbose = F)

# Intermediate Zones (MSOA equivalents) for Scotland
msoa_sco = readOGR(dsn = file.path(data.dir, "Boundaries", "Scotland"), 
                   layer = "SG_IntermediateZone_Bdry_2011",
                   verbose = F)

# Super Output Areas (LSOA equivalents) for Northern Ireland
lsoa_ni = readOGR(dsn = file.path(data.dir, "Boundaries", "NI/NI SOA 2011"), 
                  layer = "SOA2011",
                  verbose = F)

# convert to web map CRS and simplify boundaries
map_proj = CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
msoa_eng = spTransform(msoa_eng, map_proj)
msoa_ew = spTransform(msoa_ew, map_proj)
msoa_sco = spTransform(msoa_sco, map_proj)
lsoa_ni = spTransform(lsoa_ni, map_proj)

msoa_eng = ms_simplify(msoa_eng)
msoa_ew = ms_simplify(msoa_ew)
msoa_sco = ms_simplify(msoa_sco)
lsoa_ni = ms_simplify(lsoa_ni)


##
## load risk data - run the code in https://github.com/britishredcrosssociety/brc-risk-maps to generate these files
##
risk_eng_msoa = read_csv(file.path(data.dir, "Risk maps", "England - MSOA - risks.csv"))
risk_wal_msoa = read_csv(file.path(data.dir, "Risk maps", "Wales - MSOA - risks.csv"))
risk_sco_msoa = read_csv(file.path(data.dir, "Risk maps", "Scotland - MSOA - risks.csv"))
risk_ni_lsoa  = read_csv(file.path(data.dir, "Risk maps", "NI - LSOA - risks.csv"))


########################################################################################################
## Make GeoJSON containing all MSOA-level risks
## - fires (not Wales or NI)
## - floods (not Scotland)
## - loneliness
## - health deprivation
## - living alone
## - IMD
##

##
## join risk data into geographies
##
risk_msoa_eng = msoa_eng %>% 
  select(msoa11cd) %>% 
  left_join(risk_eng_msoa %>% rename(fires_denom = n_people),  # need to rename the fire denominator so it's consistent across countries
            by = c("msoa11cd" = "MSOA11CD")) %>% 
  rename(geog_id = msoa11cd, name = MSOA11NM) %>% 
  mutate(geog_type = "MSOA") %>% 
  select(geog_id, geog_type, name, everything())

risk_msoa_wal = msoa_ew %>% 
  filter(str_sub(msoa11cd, 1, 1) == "W") %>% 
  select(msoa11cd) %>% 
  left_join(risk_wal_msoa,
            by = c("msoa11cd" = "MSOA11CD")) %>% 
  rename(geog_id = msoa11cd, name = MSOA11NM) %>% 
  mutate(geog_type = "MSOA") %>% 
  mutate(n_fires = NA, fires_denom = NA, dens_fires = NA, fires_q_name = NA, fires_q = NA) %>%   # add missing columns (so all geographies can be joined)
  select(geog_id, geog_type, name, everything())

risk_msoa_sco = msoa_sco %>% 
  select(InterZone) %>% 
  left_join(risk_sco_msoa %>% rename(fires_denom = n_dwellings),  # need to rename the fire denominator so it's consistent across countries
            by = c("InterZone" = "MSOA11CD")) %>% 
  rename(geog_id = InterZone, name = MSOA11NM) %>% 
  mutate(geog_type = "Intermediate Zone") %>% 
  mutate(n_people_flood = NA, floods_q_name = NA, floods_q = NA) %>%   # add missing columns (so all geographies can be joined)
  select(geog_id, geog_type, name, everything())

risk_lsoa_ni = lsoa_ni %>% 
  select(SOA_CODE) %>% 
  left_join(risk_ni_lsoa %>% rename(Worst_Health_decile = Health_decile, Worst_IMD_decile = IMD_decile),  # need to rename these
            by = c("SOA_CODE" = "LSOA11CD")) %>% 
  rename(geog_id = SOA_CODE, name = LSOA11NM) %>% 
  mutate(geog_type = "Super Output Area") %>% 
  mutate(n_fires = NA, fires_denom = NA, dens_fires = NA, fires_q_name = NA, fires_q = NA) %>%   # add missing columns (so all geographies can be joined)
  select(geog_id, geog_type, name, everything())

risk_geog = rbind(risk_msoa_eng, risk_msoa_wal, risk_msoa_sco, risk_lsoa_ni, makeUniqueIDs = T)

# convert to geojson
risk_json = geojson_json(risk_geog)

# save as geoJSON
geojson_write(risk_json, file = "../data/risks-msoa.geojson")
