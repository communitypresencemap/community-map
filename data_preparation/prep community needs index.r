##
## Create GeoJSONs for the Community Needs Index
## - one containing the Index and its domains for all Wards
## - one containing only left-behind areas
##
## More info: https://ocsi.uk/2020/01/15/community-needs-index-your-questions-answered/
## The data isn't public but OCSI can provide it to social purpose organisations - email them for info
##
library(tidyverse)
library(readxl)
library(sf)
library(geojsonio)
library(brclib)  # use `devtools::install_github("matthewgthomas/brclib")` if you need to install this

source("init.r")

##
## Load data
##
# wards = read_sf("https://opendata.arcgis.com/datasets/d2dce556b4604be49382d363a7cade72_0.geojson")  # 2019 boundaries
# wards = read_sf("https://opendata.arcgis.com/datasets/a0b43fe01c474eb9a18b6c90f91664c2_3.geojson")  # 2018 boundaries
wards = read_sf("https://opendata.arcgis.com/datasets/07194e4507ae491488471c84b23a90f2_3.geojson")  # 2017 boundaries

index_scores = read_excel(file.path(input.data.dir, "Community Needs Index", "Phase 2 Community Needs Index Score and Rank.xlsx"))
index_domains = read_excel(file.path(input.data.dir, "Community Needs Index", "Community Needs Index domain scores.xlsx"))
left_behind = read_excel(file.path(input.data.dir, "Community Needs Index", "Phase 2 Left Behind Areas.xlsx"))

##
## Merge index scores, domains and left-behind areas into one dataframe
##
left_behind$left_behind = 1  # flag which wards are 'left behind'

index = index_domains %>% 
  left_join(index_scores %>% select(`Ward Code`, `Community Need Score Phase 2 Score`, `Community Needs Index Phase 2 Rank`),
            by = "Ward Code") %>% 
  
  left_join(left_behind %>% select(`Ward Code`, left_behind),
            by = "Ward Code")

##
## Calculate risk quantiles
##
index = index %>%
  add_risk_quantiles("Civic Assets score", output.col = "civic_assets") %>%
  add_risk_quantiles("Connectedness score", output.col = "connectedness") %>%
  add_risk_quantiles("Engaged community score", output.col = "engaged") %>%
  add_risk_quantiles("Community Need Score Phase 2 Score", output.col = "need_index")

# left_behind = left_behind %>%
#   add_risk_quantiles("Community Need Score Phase 2 Score", output.col = "need_index")

##
## Merge with map
##
index_wards = wards %>% 
  left_join(index, by = c("wd17cd" = "Ward Code")) %>% 
  select(civic_assets_q, connectedness_q, engaged_q, need_index_q, left_behind, wd17cd, wd17nm) %>% 
  filter(!is.na(need_index_q))

# left_behind_wards = wards %>% 
#   left_join(index, by = c("wd17cd" = "Ward Code")) %>% 
#   select(wd17cd, wd17nm, need_index_q) %>% 
#   filter(!is.na(need_index_q))

##
## Save GeoJSONs
##
# convert to geojson
index_json = geojson_json(index_wards)
# left_behind_json = geojson_json(left_behind_wards)

# save as geoJSON
geojson_write(index_json, file = file.path(output.data.dir, "community-need.geojson"))
# geojson_write(left_behind_json, file = file.path(output.data.dir, "left-behind.geojson"))
