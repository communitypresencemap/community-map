##
## Prepare demographic data:
## - people living with long-term health conditions
## - people living alone
## - proficiency in the English language
##
library(tidyverse)
library(brclib)
library(sf)
library(geojsonio)
library(rmapshaper)

# process all the data
source("demographics/prep living alone.r")
source("demographics/prep long-term health conditions.r")
source("demographics/prep proficiency in English.r")

# load Lower-layer Super Output Areas (and devolved equivalents)
lsoa = read_sf(file.path(input.data.dir, "Boundaries", "UK_LSOAs.shp"))
lsoa = select(lsoa, lsoa11cd, lsoa11nm)

# merge everything together
demographics = lsoa %>% 
  left_join(ltc_uk,   by = c("lsoa11cd" = "geography code")) %>% 
  left_join(alone_uk, by = c("lsoa11cd" = "geography code")) %>% 
  left_join(eng_uk,   by = c("lsoa11cd" = "geography code"))

# keep only relevant columns
demographics = demographics %>% 
  select(starts_with("prop_all_limited"), starts_with("prop_older_limited"), starts_with("n_older_alone"), starts_with("prop_alone"), starts_with("no_English"), 
         lsoa11cd, lsoa11nm)

# simplify shapes
demographics = ms_simplify(demographics, keep = 0.01, keep_shapes = T)

# reproject to WGS84/EPSG:4326
demographics = st_transform(demographics, crs = 4326)

# convert to geojson
demographics_json = geojson_json(demographics)

# save as geoJSON
geojson_write(demographics_json, file = file.path(output.data.dir, "demographics.geojson"))
