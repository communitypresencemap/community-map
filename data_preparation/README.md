# Community Presence Map - Data Preparation Scripts

## Overview

This page describes the steps that are needed to create the .geojson files before the tilesets can be made.

## Requirements

* R

## Data

* Risks relating to [disasters, emergencies, displacement, migration and health inequalities](https://github.com/britishredcrosssociety/brc-risk-maps)
* [Indices of Multiple Deprivation](https://github.com/matthewgthomas/IMD)
* [Community Need Index](https://ocsi.uk/2020/01/15/community-needs-index-your-questions-answered/)
* [Destitution](https://www.jrf.org.uk/file/51422/download?token=W92ukE_4&filetype=full-report)
* Demographics
* Hospitals

### Risk maps
Run `prep risk maps - Local Authority.r` to generate `risks-lad.geojson`.

Run `prep risk maps - MSOA.r` to generate `risks-msoa.geojson`.

### Community Need Index
Run `prep community needs index.r` to generate `community-need.geojson`. Note this data isn't public but OCSI can provide it to social purpose organisations - email them for info.

### Deprivation
Run `prep deprivation.r` to generate `deprivation_uk.geojson`.

### Destitution
...

### Demographics
Run `prep demographics.r` to generate `demographics.geojson`.

### Hospitals
Run `prep hospitals.r` to generate `hospitals.geojson`.
