##
## Create GeoJSON for the the Indices of Deprivation 
##
library(tidyverse)
library(brclib)  # `devtools::install_github("matthewgthomas/brclib")` if required
library(rmapshaper)
library(geojsonio)

source("init.r")

##
## Load LSOAs
##
lsoas = get_LSOAs()  # this takes some time

lsoas = ms_simplify(lsoas, keep = 0.02, keep_shapes = TRUE)

##
## Merge deprivation data into LSOAs
##
# load latest IMD data for UK from https://github.com/matthewgthomas/IMD
imd_uk = load_IMD()

lsoas_IMD = lsoas %>% 
  left_join(imd_uk %>% select(-Name), by = c("Code" = "LSOA"))

##
## Save GeoJSONs
##
# convert to geojson
imd_json = geojson_json(lsoas_IMD)

# save as geoJSON
geojson_write(imd_json, file = file.path(output.data.dir, "deprivation_uk.geojson"))
