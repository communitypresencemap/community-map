##
## Proportion of people living alone in LSOAs - from 'household composition' tables in censuses 
##
## England and Wales: QS112EW (Household composition - People), from https://www.nomisweb.co.uk/census/2011/qs112ew
## Scotland: QS112SC (Household composition - People), from https://www.scotlandscensus.gov.uk/ods-web/download/getDownloadFile.html?downloadFileIds=SNS%20Data%20Zone%202011%20blk
## NI: QS110NI (Household Composition - Usual Residents), from https://www.nisra.gov.uk/statistics/2011-census/results/quick-statistics --> http://www.ninis2.nisra.gov.uk/Download/Census%202011/Quick%20Statistics%20Tables%20(statistical%20geographies).zip
##
library(tidyverse)
library(readxl)
library(brclib)

source("init.r")

##
## load data
##
alone_ew = read_csv(file.path(input.data.dir, "Census/England and Wales", "QS112EW - Household composition - People.csv"))
alone_sco = read_csv(file.path(input.data.dir, "Census/Scotland/SNS Data Zone 2011", "QS112SC.csv"))
alone_ni = read_csv(file.path(input.data.dir, "Census/NI/Quick Statistics Tables (statistical geographies)/SUPER OUTPUT AREAS", "QS110NIDATA0.CSV"))

##
## NI's data has codes for column names - replace codes with column names
##
alone_ni_cols = read_csv(file.path(input.data.dir, "Census/NI/Quick Statistics Tables (statistical geographies)/SUPER OUTPUT AREAS", "QS110NIDESC0.CSV"))

names(alone_ni) = c("geography code", alone_ni_cols$ColumnVariableDescription)

##
## calculate proportions of people living alone in each census/country
##
alone_ew = alone_ew %>% 
  mutate(prop_alone = (`Household Composition: One person household: Total; measures: Value` + `Household Composition: One family only: Lone parent: Total; measures: Value`) / `Household Composition: All categories: Household composition; measures: Value`) %>% 
  select(`geography code`, prop_alone, 
         n_people_total  = `Household Composition: All categories: Household composition; measures: Value`,
         n_one_person_hh = `Household Composition: One person household: Total; measures: Value`, 
         n_lone_parents  = `Household Composition: One family only: Lone parent: Total; measures: Value`,
         n_older_alone   = `Household Composition: One person household: Aged 65 and over; measures: Value`) %>% 
  
  add_risk_quantiles("prop_alone", quants = 10) %>% 
  add_risk_quantiles("n_older_alone", quants = 10)

alone_sco = alone_sco %>% 
  filter(X1 != "S92000003") %>%   # ignore Scottish total
  mutate(prop_alone = (`One person household` + `One family household: Lone parent family`) / `All people in households`) %>% 
  select(`geography code` = X1, prop_alone,
         n_people_total  = `All people in households`,
         n_one_person_hh = `One person household`,
         n_lone_parents  = `One family household: Lone parent family`,
         n_older_alone   = `One person household: Aged 65 and over`) %>% 
  
  add_risk_quantiles("prop_alone", quants = 10) %>% 
  add_risk_quantiles("n_older_alone", quants = 10)

alone_ni = alone_ni %>% 
  mutate(prop_alone = (`One person household: Total` + `One family and no other people: Lone parent: Total`) / `All usual residents in households`) %>% 
  select(`geography code`, prop_alone,
         n_people_total  = `All usual residents in households`,
         n_one_person_hh = `One person household: Total`,
         n_lone_parents  = `One family and no other people: Lone parent: Total`,
         n_older_alone   = `One person household: Aged 65+ years`) %>% 
  
  add_risk_quantiles("prop_alone", quants = 10) %>% 
  add_risk_quantiles("n_older_alone", quants = 10)

##
## combine into single dataset and save
##
alone_uk = bind_rows(alone_ew, alone_sco, alone_ni)

write_csv(alone_uk, file.path(input.data.dir, "Census", "UK - Household composition - People - LSOA.csv"))

rm(alone_ew, alone_sco, alone_ni, alone_ni_cols)
