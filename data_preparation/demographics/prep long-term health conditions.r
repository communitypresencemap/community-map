##
## Long-term health conditions
## - proportion of older people with LTCs
## - proportion of older people living alone with LTCs?
##
## England/Wales LSOA: LC3302EW (Long-term health problem or disability by general health by sex by age)
## Scotland DZ: LC3101SC (Long-term health problem or disability by age)
## NI SOA: LC3105NI (Long-Term Health Problem or Disability by Age)
## 
## England/Wales LSOA: LC1301EW (Household composition by number of people in household with a long-term health problem or disability)
## Scotland DZ: LC1301SC (Household composition by number of people in household with a long-term health problem or disability)
## NI: *not available*
##
library(tidyverse)
library(readxl)
library(brclib)

source("init.r")

##
## load data
##
ltc_ew = read_csv(file.path(input.data.dir, "Census/England and Wales", "LC3302EW - Long-term health problem or disability by general health by sex by age - LSOA.csv"))
ltc_sco = read_csv(file.path(input.data.dir, "Census/Scotland/SNS Data Zone 2011", "LC3101SC.csv"))
ltc_ni = read_csv(file.path(input.data.dir, "Census/NI/Local Characteristic Tables (statistical geographies)/SUPER OUTPUT AREAS", "LC3105NIDATA0.CSV"))

# load household composition tables - can lookup no. people with long-term conditions who live alone
# (except for NI, which doesn't publish this table)
# ltc_hh_ew = read_csv(file.path(input.data.dir, "Census/England and Wales", "LC1301EW - Household composition by number of people in household with a long-term health problem or disability - LSOA.csv"))
# ltc_hh_sco = read_csv(file.path(input.data.dir, "Census/Scotland/SNS Data Zone 2011", "LC1301SC.csv"))


##
## NI's data has codes for column names - replace codes with column names
##
ltc_ni_cols = read_csv(file.path(input.data.dir, "Census/NI/Local Characteristic Tables (statistical geographies)/SUPER OUTPUT AREAS", "LC3105NIDESC0.CSV"))

names(ltc_ni) = c("geography code", ltc_ni_cols$ColumnVariableDescription)


##
## summarise
##
ltc_ew = ltc_ew %>% 
  mutate(n_older_total = `Sex: All persons; Age: Age 65 to 74; Disability: All categories: Long-term health problem or disability; General Health: All categories: General health; measures: Value` +
           `Sex: All persons; Age: Age 75 to 84; Disability: All categories: Long-term health problem or disability; General Health: All categories: General health; measures: Value` + 
           `Sex: All persons; Age: Age 85 and over; Disability: All categories: Long-term health problem or disability; General Health: All categories: General health; measures: Value`,
         
         n_older_limited = `Sex: All persons; Age: Age 65 to 74; Disability: Day-to-day activities limited a lot; General Health: All categories: General health; measures: Value` +
         `Sex: All persons; Age: Age 75 to 84; Disability: Day-to-day activities limited a lot; General Health: All categories: General health; measures: Value` +
         `Sex: All persons; Age: Age 85 and over; Disability: Day-to-day activities limited a lot; General Health: All categories: General health; measures: Value`) %>% 
  
  select(`geography code`, n_older_total, n_older_limited,
         n_all_total = `Sex: All persons; Age: All categories: Age; Disability: All categories: Long-term health problem or disability; General Health: All categories: General health; measures: Value`,
         n_all_limited = `Sex: All persons; Age: All categories: Age; Disability: Day-to-day activities limited a lot; General Health: All categories: General health; measures: Value`) %>% 
  
  mutate(prop_all_limited = ifelse(n_all_total > 0, n_all_limited / n_all_total, 0),
         prop_older_limited = ifelse(n_older_total > 0, n_older_limited / n_older_total, 0)) %>% 
  
  add_risk_quantiles("prop_all_limited", quants = 10) %>% 
  add_risk_quantiles("prop_older_limited", quants = 10) 
  
ltc_sco = ltc_sco %>% 
  # rename columns and filter out unnecessary rows
  rename(`geography code` = X1, age = X2, Total = `All people`) %>%
  filter(`geography code` != "S92000003") %>% 
  filter(age %in% c("All people", "65 to 74", "75 to 84", "85 and over")) %>% 
  select(-`Day-to-day activities limited a little`, -`Day-to-day activities not limited`) %>% 
  
  # label all ages 65+ as "Older"
  mutate(age = ifelse(age != "All people", "Older", age)) %>% 
  
  # each data zone has three rows for 'Older': sum them
  group_by(`geography code`, age) %>% 
  summarise(Total = sum(Total), Limited = sum(`Day-to-day activities limited a lot`)) %>% 
  ungroup() %>% 
  
  # we want to end up with one column for each combination of {All people, Older} and {Total, Limited}
  gather(type, n, Total:Limited) %>%  # convert the two numeric cols into long format
  unite("tmp", c("age", "type")) %>%  # merge the two labels into one
  spread(tmp, n) %>%                  # spread into four cols
  
  rename(n_all_total = `All people_Total`,
         n_all_limited = `All people_Limited`,
         n_older_total = Older_Total,
         n_older_limited = Older_Limited) %>% 
  
  mutate(prop_all_limited = ifelse(n_all_total > 0, n_all_limited / n_all_total, 0),
         prop_older_limited = ifelse(n_older_total > 0, n_older_limited / n_older_total, 0)) %>%
  
  add_risk_quantiles("prop_all_limited", quants = 10) %>% 
  add_risk_quantiles("prop_older_limited", quants = 10) 

ltc_ni = ltc_ni %>% 
  mutate(n_older_total = `Age 65 to 74, All categories: Disability` + `Age 75 and over, All categories: Disability`,
         n_older_limited = `Age 65 to 74, Day-to-day activities limited a lot` + `Age 75 and over, Day-to-day activities limited a lot`) %>% 
  
  select(`geography code`, n_older_total, n_older_limited,
         n_all_total = `All categories: Age, All categories: Disability`,
         n_all_limited = `All categories: Age, Day-to-day activities limited a lot`) %>% 
  
  mutate(prop_all_limited = ifelse(n_all_total > 0, n_all_limited / n_all_total, 0),
         prop_older_limited = ifelse(n_older_total > 0, n_older_limited / n_older_total, 0)) %>%
  
  add_risk_quantiles("prop_all_limited", quants = 10) %>% 
  add_risk_quantiles("prop_older_limited", quants = 10) 

##
## combine into single dataset and save
##
ltc_uk = bind_rows(ltc_ew, ltc_sco, ltc_ni)

write_csv(ltc_uk, file.path(input.data.dir, "Census", "UK - Long-term conditions - LSOA.csv"))

rm(ltc_ew, ltc_sco, ltc_ni, ltc_ni_cols)
