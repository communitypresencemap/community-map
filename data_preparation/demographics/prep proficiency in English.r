##
## Proficiency in English
##
##
## England/Wales LSOA: QS205EW - Proficiency in English
## Scotland DZ: QS205SC
## NI SOA: QS211NI
##
library(tidyverse)
library(readxl)
library(brclib)

source("init.r")

##
## load data
##
eng_ew = read_csv(file.path(input.data.dir, "Census/England and Wales", "QS205EW - Proficiency in English - LSOA.csv"))
eng_sco = read_csv(file.path(input.data.dir, "Census/Scotland/SNS Data Zone 2011", "QS205SC.csv"))
eng_ni = read_csv(file.path(input.data.dir, "Census/NI/Quick Statistics Tables (statistical geographies)/SUPER OUTPUT AREAS", "QS211NIDATA0.CSV"))


##
## NI's data has codes for column names - replace codes with column names
##
eng_ni_cols = read_csv(file.path(input.data.dir, "Census/NI/Quick Statistics Tables (statistical geographies)/SUPER OUTPUT AREAS", "QS211NIDESC0.CSV"))

names(eng_ni) = c("geography code", eng_ni_cols$ColumnVariableDescription)


##
## summarise
##
eng_ew = eng_ew %>% 
  select(`geography code`, no_English = `Proficiency in English: Main language is not English (English or Welsh in Wales): Cannot speak English; measures: Value`) %>% 
  add_risk_quantiles("no_English", quants = 10)

eng_sco = eng_sco %>% 
  filter(X1 != "S92000003") %>%   # ignore Scottish total
  select(`geography code` = X1, no_English = `Does not speak English at all`) %>% 
  add_risk_quantiles("no_English", quants = 10)

eng_ni = eng_ni %>% 
  select(`geography code`, no_English = `Main language is not English: Cannot speak English: Aged 3+ years`) %>% 
  add_risk_quantiles("no_English", quants = 10)


##
## combine into single dataset and save
##
eng_uk = bind_rows(eng_ew, eng_sco, eng_ni)

write_csv(eng_uk, file.path(input.data.dir, "Census", "UK - English language proficiency - LSOA.csv"))

rm(eng_ew, eng_sco, eng_ni, eng_ni_cols)