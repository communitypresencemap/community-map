##
## generate dummy data for staff, vols, CRVs, properties (not shops)
##
library(tidyverse)
library(geojsonio)
library(jsonlite)
library(readxl)
library(stringr)

# for map layers
library(rgdal)
library(rmapshaper)
library(spdplyr)

source("init.r")

# download and unzip latest postcode directory (this takes a while!)
download.file("https://www.arcgis.com/sharing/rest/content/items/ae45e39fdafe4520bba99c760c303109/data",
              "postcodes.zip")

unzip("postcodes.zip", exdir = "postcodes")

# function to load postcodes as and when needed
load_postcodes = function() {
  read_csv(file.path(data.dir, "Postcodes", "National_Statistics_Postcode_Lookup - BRC.csv"),
           col_types = cols(
             Postcode = col_character(),
             Longitude = col_double(),
             Latitude = col_double(),
             Country = col_character(),
             CountryName = col_character(),
             `Output Area` = col_character(),
             LSOA = col_character(),
             `Local Authority Code` = col_character(),
             `Primary Care Trust` = col_character(),
             `Rural or Urban?` = col_character(),
             `Rural Urban classification` = col_character(),
             IMD = col_integer(),
             IMD.Decile = col_integer(),
             IMD.Decile.Name = col_character(),
             `Rurality index` = col_double()
           ))
}

if (!exists("postcodes")) postcodes = load_postcodes()

# volunteer basis
basis = c("Occasional", "Regular", "Skills Gap Outstanding", "Contractor")

# divisions and directorates
div_dir = structure(list(Directorate = c("Fundraising", "UK Operations", "Fundraising", 
                                         "UK Operations", "UK Operations", "UK Operations", "UK Operations", 
                                         "UK Operations", "Register", "UK Operations", "People & Learning", 
                                         "Communications & Engagement", "Fundraising", "Information and Digital Technology", 
                                         "Chief Executive's Office", "Finance Planning & Resources", "UK Operations", 
                                         "nk Group Volunteers", "UK Operations", "UK Operations", "UK Operations", 
                                         "International", "Strategic Change", "UK Operations"),
                         Team = c("Retail", "Event First Aid", "Community Fundraising", 
                                  "Crisis Response", "Independent Living", "Mobility Aids", "Restoring Family Links", 
                                  "Refugee Support", "Register", "Education", "People & Learning", 
                                  "Communications & Engagement", "Fundraising", "Information and Digital Technology", 
                                  "Chief Executive's Office", "Finance Planning & Resources", "UK Ops Finance, HR etc.", 
                                  "UK Ops Finance, HR etc.", "Innovation and Insight", "Ambulance Support", 
                                  "Community Equipment Services", "International", "Strategic Change", 
                                  "CRV")),
                    row.names = c(NA, -24L), 
                    class = c("tbl_df", "tbl", "data.frame"), .Names = c("Division", "Directorate"))

##
## 20,000 traditional vols + 6,000 CRVs
##
# pick random postcodes, random basis and random divisions/directorates
n.vols = 26000

# set up basic dataframe
vols.sim = data_frame(
  id = 1:n.vols,
  Basis = sample(basis, n.vols, replace = T)
)

# append divisions and directorates
vols.sim = bind_cols(
  vols.sim,
  div_dir[ sample(nrow(div_dir), n.vols, replace = T), ],
  postcodes[ sample(nrow(postcodes), n.vols, replace = T), c("Latitude", "Longitude")]
) %>% 
  na.omit()


##
## 3,000 staff
##
n.staff = 3000

staff.sim = bind_cols(
  postcodes[ sample(nrow(postcodes), n.staff, replace = T), c("Latitude", "Longitude")],
  div_dir[ sample(nrow(div_dir), n.staff, replace = T), ]
)

staff.sim$Level = rpois(n.staff, 3) + 1

staff.sim$UKO = rbinom(n.staff, size = 1, prob = 0.22)
staff.sim$SSC = rbinom(n.staff, size = 1, prob = 0.05)
staff.sim$RCT = rbinom(n.staff, size = 1, prob = 0.07)

##
## 300 properties (not shops)
##
n.props = 300
props.sim = bind_cols(
  data_frame(UPC = 1:n.props,
             Description = "Dummy property",
             Address = "Dummy address"),
  postcodes[ sample(nrow(postcodes), n.props, replace = T), c("Latitude", "Longitude")]
)


###################################################################################
## Save everything
##
staff.json = toJSON(staff.sim)

staff.js = paste0("var staff = ", staff.json)
write_lines(staff.js, path = "../web app/data/staff-sim.js")

vols.json = toJSON(vols.sim)
vols.js = paste0("var vols = ", vols.json)
write_lines(vols.js, path = "../web app/data/vols-sim.js")

props.json = toJSON(props.sim)
props.js = paste0("var props = ", props.json)
write_lines(props.js, path = "../web app/data/props-sim.js")



