##
## Prepare data for loading into pure html/js version of the web app
##
library(tidyverse)
library(geojsonio)
library(jsonlite)
library(readxl)
library(stringr)

# for map layers
library(rgdal)
library(rmapshaper)
library(spdplyr)

data.dir = "../../../../Data"

# function to load postcodes as and when needed
load_postcodes = function() {
  read_csv(file.path(data.dir, "Postcodes", "National_Statistics_Postcode_Lookup - BRC.csv"),
           col_types = cols(
             Postcode = col_character(),
             Longitude = col_double(),
             Latitude = col_double(),
             Country = col_character(),
             CountryName = col_character(),
             `Output Area` = col_character(),
             LSOA = col_character(),
             `Local Authority Code` = col_character(),
             `Primary Care Trust` = col_character(),
             `Rural or Urban?` = col_character(),
             `Rural Urban classification` = col_character(),
             IMD = col_integer(),
             IMD.Decile = col_integer(),
             IMD.Decile.Name = col_character(),
             `Rurality index` = col_double()
           ))
}


#############################################################################################
## Volunteers
##
vols.trad = read_csv(file.path(data.dir, "Volunteers/volunteers - latest.csv")) %>%
  filter(!is.na(Latitude) & !is.na(Longitude)) %>% 
  rename(Role = `Job Family`, Team = `Level6:Structure`, Directorate = `Level3:Structure`, Basis = `Basis:Posn`) %>% 
  mutate(Role = str_sub(Role, start=7)) %>%   # remove "VOL - " from start of Role
  
  # reorganise Teams
  mutate(Directorate = str_sub(Directorate, start = 5), # remove directorate code
         Team = str_sub(Team, start = 4)) %>%   # remove Team code (first three letters) from start of Team
  
  # keep UK services and fundraising separate but otherwise set Team to the directorate
  mutate(Team = case_when(
    Directorate == "UK Operations" ~ Team,
    Directorate == "Fundraising" & grepl("Retail", Team) ~ "Retail",
    Directorate == "Fundraising" & grepl("Community", Team) ~ "Community Fundraising",
    TRUE ~ Directorate
  )) %>% 
  
  # combine tutor, adult education, youth education into one role
  mutate(Team = fct_collapse(Team,
                             Education = c("Crisis Education", "Education Management & Marketing"),
                             `UK Ops Finance, HR etc.` = c("Area Admin", "Properties", "UK Services Development & Management", "nk Group Volunteers"),  # the last entry is actually "Link Group Volunteers"; not really sure who they are so putting them in here; actually is just one person
                             `Communications & Engagement` = "Communications & Advocacy"
  )) %>% 
  
  mutate(Directorate = ifelse(Directorate == "Communications & Advocacy" ,"Communications & Engagement", Directorate)) %>% 
  
  # combine tutor, adult education, youth education into one role
  # mutate(Role = fct_collapse(Role,
  #                            Education = c("Adult Education", "Tutor", "Youth Education"),
  #                            `Crisis Response` = "Emergency Response",
  #                            ``
  # )) %>% 
  select(id = `Person ID`, Directorate, Team, Basis, Latitude, Longitude)

# load CRVs
# first run `clean volunteer data - CRV.r` in P:\Operations\Innovation & Insight\Insight\Data science\Projects\General code
crv = read_csv(file.path(data.dir, "Volunteers/CRVs - latest.csv")) %>% 
  #filter(maps_age < 100) %>%   # ignore people over 100 -- they are the dummy accounts
  filter(!is.na(Latitude) & !is.na(Longitude)) %>% 
  mutate(id = as.character(id)) %>% 
  mutate(Role = "CRV",
         Basis = "Regular",
         Team = "CRV",
         Directorate = "UK Operations") %>% 
  select(id, Directorate, Team, Basis, Latitude, Longitude)

# combine
vols  = bind_rows(vols.trad, crv)

##
## save
##
# save as geoJSON file
geojson_write(vols, lat = "Latitude", lon = "Longitude", file = "../Web app/data/vols.geojson")

#... and as standard csv
# write_csv(vols, "../Web app/data/vols.csv")

#... and as a javascript variable declaration
vols.json = toJSON(vols)  # toJSON(vols, dataframe = "values")
vols.js = paste0("var vols = ", vols.json)

write_lines(vols.js, path = "../web app/data/vols.js")


#############################################################################################
## Staff
##
# staff = read_csv("../Data/staff_final.csv") %>% 
staff = read_csv(file.path(data.dir, "Staff/staff - latest.csv")) %>%
  rename(Team = `Level6:Structure`, Directorate = `Level3:Structure`) %>% 
  
  # keep only regular staff
  filter(Basis != "Irregular") %>% 
  
  # extract level number 
  mutate(Level = str_extract(`Grade/Payscale`, "(Level|Grade) [0-9]")) %>%   # get level or grade with number
  mutate(Level = as.integer(str_extract(Level, "[0-9]"))) %>%   # keep only the number
  mutate(Level = ifelse(`Grade/Payscale` == "CEO", 9, Level)) %>%  # CEO is a special case - make them level 9

  # flag if people work in UKO, SSC and/or RCT
  mutate(RCT = ifelse(grepl(".*Red Cross Training", `Level5:Structure`), 1L, 0L),
         UKO = ifelse(grepl(".*UK Office", `Location`), 1L, 0L),
         SSC = ifelse(grepl(".*Shared Service Centre", `Location`), 1L, 0L)) %>% 
    
  # reorganise Teams
  mutate(Directorate = str_sub(Directorate, start = 5), # remove directorate code
         Team = str_sub(Team, start = 4)) %>%   # remove Team code (first three letters) from start of Team
  
  # keep UK services and fundraising separate but otherwise set Team to the directorate
  mutate(Team = case_when(
    Directorate == "UK Operations" ~ Team,
    Directorate == "Fundraising" & grepl("Retail", Team) ~ "Retail",
    Directorate == "Fundraising" & grepl("Community", Team) ~ "Community Fundraising",
    TRUE ~ Directorate
  )) %>% 
  
  # combine tutor, adult education, youth education into one role
  mutate(Team = fct_collapse(Team,
                                 Education = c("Crisis Education", "Education Development", "Education Management & Marketing"),
                                 `UK Ops Finance, HR etc.` = c("Properties", "UK Services Development & Management")
                                 
                                 # Fundraising = c("Community Fundraising", "Fundraising", "Fundraising Strategy Unit", "Major Donors and Events", "Management of Fundraising"),
                                 # International = c("International DM", "International Management", "International Partnership Development", "International Programmes"),
                                 # MIS = c("- MIS", "MIS"),
                                 # Retail = c("Retail Britcross", "Retail National", "Retail North", "Retail North Distribution", "Retail South", "Retail South Distribution"),
  )) %>% 
  
  select(Latitude, Longitude, Directorate, Team, Level, UKO, SSC, RCT)

staff = na.omit(staff)

##
## save
##
# save as geoJSON file
geojson_write(staff, lat = "Latitude", lon = "Longitude", file = "../Web app/data/staff.geojson")

# convert to javascript variable declaration
staff.json = toJSON(staff)  # toJSON(vols, dataframe = "values")
staff.js = paste0("var staff = ", staff.json)

write_lines(staff.js, path = "../web app/data/staff.js")


#############################################################################################
## List of service lines and other BRC divisions
## - save in .json format for automatically populating the dropdown list
##
divisions = bind_rows(
  vols %>% select(Directorate, Division),
  staff %>% select(Directorate, Division)
) %>% 
  distinct() %>% 
  arrange(Directorate, Division)


# convert to a JS variable declaration
divisions.json = toJSON(divisions)
divisions.js = paste0("var divisionsOptions = ", divisions.json)

write_lines(divisions.js, path = "../web app/data/divisions.js")


#############################################################################################
## Services and shops
##
services = read_csv("../Data/services.csv")
service_types = read_csv("../Data/service types.csv")

services = services %>% 
  left_join(service_types, by=c("Service" = "ServiceName")) %>% 
  filter(!is.na(Latitude) & !is.na(Longitude)) %>%   # remove entries we can't locate
  rename(Phone = `Phone Number`, Area = `UK Area`)

shops = services %>% 
  filter(Service == "Shop")

services = services %>% 
  filter(Service != "Shop")

##
## save
##
# convert to javascript variable declaration
services.json = toJSON(services)
services.js = paste0("var services = ", services.json)

# shops.json = toJSON(shops)
# shops.js = paste0("var shops = ", shops.json)

write_lines(services.js, path = "../web app/data/services.js")
# write_lines(shops.js, path = "../web app/data/shops.js")


#############################################################################################
## Properties
##
# Load properties
props.curr = read_excel(file.path(data.dir, "Properties/Current Properties - 1 May V1.xlsx"))

props.curr = props.curr %>% 
  rename(Postcode = `Post Code`)

# sort out Division field
props.curr = props.curr %>% 
  mutate(Division = case_when(
    Division == "CES" ~ "Community Equipment Services",
    Division == "EFA" ~ "Event First Aid",
    Division == "MAS" ~ "Mobility Aids",
    Division == "RCT" ~ "Education",
    Division == "RCT & UK Ops" ~ "Education",
    Division == "UK Ops & RCT" ~ "Education",
    Division == "UK Ops, RCT & Region" ~ "Education",
    `Red Cross Type` == "Mobility Aids Service" ~ "Mobility Aids",
    Division == "UK Ops & Region" ~ "UK Operations",
    Division == "Retail & UK Ops" ~ "Retail,UK Operations",
    Division == "UK Ops & Retail" ~ "Retail,UK Operations",
    Division == "UK Ops" ~ "UK Operations",
    Division == "UK Ops & UKO" ~ "UK Operations",
    Division == "UK Ops, RCT & Retail" ~ "UK Operations,Education,Retail",
    Division == "UK Ops, RCT & Options for Inde" ~ "UK Operations,Education",
    Division == "UK Ops/Fundraising" ~ "UK Operations,Fundraising - Other",
    Division == "Fundraising" ~ "Fundraising - Other",
    Division == "Legacy" ~  "Fundraising - Other",
    is.na(Division) & `Red Cross Type` == "Shop" ~ "Retail",
    `Red Cross Type` == "RCT Venue" ~ "Education",
    `Red Cross Type` == "RCT Office" ~ "Education",
    `Red Cross Type` == "Retail Storage" ~ "Retail",
    `Property Description` == "Aberystwyth RSRFL off" ~ "Refugee Support,Restoring Family Links",
    #grepl("[Ss]hop", `Red Cross Type`) & is.na(Division) ~ "Retail",
    TRUE ~ Division
  ))

# sort(unique(props.curr$Division))

props.curr = props.curr %>% 
  mutate(Division = strsplit(Division, ",")) %>% 
  unnest(Division)


##
## look up locations from postcodes
##
if (!exists("postcodes")) postcodes = load_postcodes()

# the ONS data truncates 7-character postcodes to remove spaces (e.g. CM99 1AB --> CM991AB); get rid of all spaces in both datasets to allow merging
postcodes$Postcode2 = gsub(" ", "", postcodes$Postcode)
props.curr$Postcode2 = gsub(" ", "", props.curr$Postcode)

# merge
props.curr = props.curr %>% 
  left_join(postcodes, by="Postcode2")

props.curr$Postcode2 = NULL  # don't need the truncated column anymore
props.curr$Postcode.y = NULL
props.curr = rename(props.curr, Postcode = Postcode.x)

##
## separate shops from other buildings
##
# keep only useful fields and remove entries with no coordinates
props.curr = props.curr %>% 
  select(UPC, Description = `Property Description`, Address = `Property Address (Full)`, Type = `Red Cross Type`, Latitude, Longitude) %>% 
  filter(!is.na(Latitude) & !is.na(Longitude))

# shops.curr = props.curr %>% 
#   filter(grepl("[Ss]hop", Type))

props.curr = props.curr %>% 
  filter(!grepl("[Ss]hop", Type))

##
## save
##
props.json = toJSON(props.curr)
props.js = paste0("var props = ", props.json)

# shops.json = toJSON(shops.curr)
# shops.js = paste0("var shops = ", shops.json)

write_lines(props.js, path = "../web app/data/props.js")
# write_lines(shops.js, path = "../web app/data/shops.js")


#############################################################################################
## Shops
##
shops = read_excel(file.path(data.dir, "Services/Retail/Retail & CFR CONTACT DIRECTORY October 2019.xls"), sheet = "Shops")

shops = shops %>% 
  select(`Shop code`, `UPC Code`, `Shop name`, `Address 1`, `Address 2`, `Address 3`, Postcode = `Post code`, Telephone = Telephone...14, `Opening hours`, Format = Format...29)

##
## look up locations from postcodes
##
if (!exists("postcodes")) postcodes = load_postcodes()

# the ONS data truncates 7-character postcodes to remove spaces (e.g. CM99 1AB --> CM991AB); get rid of all spaces in both datasets to allow merging
postcodes$Postcode2 = gsub(" ", "", postcodes$Postcode)
shops$Postcode2 = gsub(" ", "", shops$Postcode)

# merge
shops = shops %>% 
  left_join(postcodes, by="Postcode2")

shops$Postcode2 = NULL  # don't need the truncated column anymore
shops$Postcode.y = NULL
shops = rename(shops, Postcode = Postcode.x)

##
## save
##
# convert to geojson and save
shops.geojson = geojson_json(shops)
geojson_write(shops.geojson, file = "../web app/data/shops.geojson")

shops.json = toJSON(shops)
shops.js = paste0("var shops = ", shops.json)

write_lines(shops.js, path = "../web app/data/shops.js")


#############################################################################################
## Services from website
##
services = read_excel(file.path(data.dir, "Services/Service Data.xlsx"))

services = services %>% 
  select(-GetDirectionsText, -CallBackFormLink, -CallBackText, -CallBackFormEmailAddress, -ReserveFormLink) %>% 
  replace_na(list(Services = "unknown", Address = "", Telephone = "", Monday = "", Tuesday = "", Wednesday = "", Thursday = "", Friday = "", Saturday = "", Sunday = "")) %>% 
  
  separate(LatLon, c("Latitude", "Longitude"), sep = ",", remove = T) %>%   # split latitude and longitude into separate columns
  
  # convert the list of services into an HTML list
  mutate(Services = paste0("<li>", Services, "</li>")) %>% 
  mutate(Services = str_replace_all(Services, ", ", "</li><li>")) %>% 
  
  separate(ServiceName, into = "ServiceType", sep = ",", remove = F)  # get types of service

services = na.omit(services)

# sort out service types
services = services %>% 
  mutate(ServiceType = case_when(
    str_detect(ServiceType, "Mobility") ~ "Mobility Aids",
    str_detect(ServiceType, "[Rr]efugee") ~ "Refugee Support",
    str_detect(ServiceType, "Connecting") ~ "Independent Living",
    str_detect(ServiceType, "International") ~ "Restoring Family Links",
    ServiceType == "Salford Food Parcels" ~ "Refugee Support",
    TRUE ~ ServiceType
  ))

##
## save
##
# convert to javascript variable declaration
services.json = toJSON(services)
services.js = paste0("var services = ", services.json)

write_lines(services.js, path = "../web app/data/services_new.js")

##
## save geojson
##
# make popup text variable
services.geojson = services %>% 
  mutate(popuptext = paste0("<b>", ServiceName, "</b><br/><br/>",
           "<b>Services:</b><ul>", Services, "</ul>",
           "<b>Address:</b> ", Address, "<br/><br/>",
           "<b>Phone number:</b> ", Telephone)) %>% 
  
  select(ServiceName, ServiceType, Latitude, Longitude, popuptext) %>% 
  geojson_json()

geojson_write(services.geojson, file = "../web app/data/services_new.geojson")


#############################################################################################
## Community Connector sites - save as geoJSON
##
# load community connector sites
# (run `.r` when these need to be updated [e.g. when there are new sites])
cc_districts = readRDS(file.path(data.dir, "Services/IL", "CC boundaries.rds"))

# lookup country based on first part of postcode
# postcodes$pc1 = str_extract(postcodes$Postcode, "[A-Z]+[0-9]+[A-Z]*")
# 
# postcodes %>% 
#   filter(startsWith(pc1, "AB10"))
# 
# cc_districts %>% 
#   left_join(postcodes %>% select(pc1, Country), by = c("Name" = "pc1"))

# convert to geojson
cc_json = geojson_json(cc_districts)

# simplify boundaries
cc_simple = ms_simplify(cc_json)

# save as geoJSON
geojson_write(cc_simple, file = "../web app/data/cc.geojson")

# need to insert `var cc = ` before the geoJSON data - can't do this and still write with geojson_write() so just reload the file as text and prepend
cc.js = read_lines("../web app/data/cc.geojson")    # load file as raw text
cc.js = paste0("var cc = ", cc.js)         # prepend JS variable declaration
write_lines(cc.js, path = "../web app/data/cc.js")  # save new file
file.remove("../web app/data/cc.geojson")                    # delete the geojson file


#############################################################################################
## Load other BRC services
##
# load services
services_il = read_csv(file.path(data.dir, "Services/IL/IL services.csv")) %>% 
  replace_na(list(Hospitals = ""))

# map data from: https://www.google.com/maps/d/viewer?mid=1MSK-8hykXSRL4Ef7ybvShjfoC_0
services_rsrfl = read_excel(file.path(data.dir, "Services/RSRFL/RSRFL.xlsx"))

# extract postcodes from addresses
# regular expression to match postcodes (allowing lowercase and unlimited spaces)
# source: https://stackoverflow.com/a/7259020
# see also: page 6 of https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/488478/Bulk_Data_Transfer_-_additional_validation_valid_from_12_November_2015.pdf
postcode_regex = "(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))"

services_rsrfl$Postcode = str_extract(services_rsrfl$`ns1:address`, postcode_regex)

services_rsrfl = services_rsrfl %>% 
  filter(name == "Description of Services")

##
## merge postcode info
##
if (!exists("postcodes")) postcodes = load_postcodes()

# the ONS data truncates 7-character postcodes to remove spaces (e.g. CM99 1AB --> CM991AB); get rid of all spaces in both datasets to allow merging
postcodes$Postcode2 = gsub(" ", "", postcodes$Postcode)
services_rsrfl$Postcode2 = gsub(" ", "", services_rsrfl$Postcode)

services_rsrfl = services_rsrfl %>% 
  left_join(postcodes, by="Postcode2")

services_rsrfl$Postcode2 = NULL  # don't need the truncated column anymore
services_rsrfl$Postcode.y = NULL
services_rsrfl = rename(services_rsrfl, Postcode = Postcode.x)


#############################################################################################
## Hospitals (mark ones we have IL contracts in)
##
# hospitals data produced during creation of NHS map (see `copy data.r` in that project)
hospitals = read_csv(file.path(data.dir, "NHS/UK hospitals.csv"))

# load IL contracts
il_contracts = read_csv(file.path(data.dir, "Services/IL", "Contract Details.csv"))
  
# lookup hospitals from il_contracts
il_hosp = il_contracts %>% 
  select(`Main Hospital`) %>% 
  distinct() %>% 
  left_join(hospitals %>% select(Name) %>% mutate(Yes = "X"),
            by = c("Main Hospital" = "Name"))

# manually collated list from any rows in `il_hosp` where the `Yes` column is NA
other_hospitals = c("Felixstowe Community Hospital", "Bluebird Lodge", "Royal Free Hospital",
                    "King's College Hospital", "Chelsea and Westminster Hospital", "West Middlesex University Hospital",
                    "St Mary's Hospital", "Charing Cross Hospital", "Macclesfield District General Hospital",
                    "Bedford Hospital North Wing", "Bedford Hospital South Wing", "Basingstoke And North Hampshire Hospital",
                    "Ystradgynlais Community Hospital")

# mark which hospitals contain BRC services
hospitals = hospitals %>%
  left_join(il_contracts %>% select(`Main Hospital`) %>% mutate(`BRC services?` = 1),
            by = c("Name" = "Main Hospital")) %>% 
  
  # some we'll have to mark manually
  mutate(`BRC services?` = case_when(
    Name %in% other_hospitals ~ 1,
    is.na(`BRC services?`) ~ 0,
    TRUE ~ `BRC services?`
  ))

# get rid of any duplicates
hospitals = hospitals %>% distinct()

# convert to geojson
hospitals_geojson = geojson_json(hospitals)
geojson_write(hospitals_geojson, file = "../web app/data/hospitals.geojson")


#############################################################################################
## Save BRC sites as geoJSON
##
services_all = bind_rows(
  # services from website
  services %>% mutate(Latitude = as.numeric(Latitude), Longitude = as.numeric(Longitude)) %>% select(Latitude, Longitude),
  services_il %>% select(Latitude, Longitude),
  services_rsrfl %>% select(Latitude, Longitude)
) %>% 
  distinct()

brc = bind_rows(
  services_all %>% mutate(BRC = "Service") %>% select(BRC, Latitude, Longitude),
  props.curr %>% mutate(BRC = "Property") %>% select(BRC, Latitude, Longitude),
  shops %>% mutate(BRC = "Shop") %>% select(BRC, Latitude, Longitude)
)

# convert to geojson
brc_json = geojson_json(brc)
services_json = geojson_json(services_all)
props_json = geojson_json(props.curr)
shops_json = geojson_json(shops)

# save as geoJSON
geojson_write(brc_json, file = "../web app/data/brc.geojson")
geojson_write(services_json, file = "../web app/data/services.geojson")
geojson_write(props_json, file = "../web app/data/props.geojson")
geojson_write(shops_json, file = "../web app/data/shops.geojson")
