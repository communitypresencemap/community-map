### Conversation

<!-- What's the story? -->

### Scenarios

<!-- What scenarios describe better the expected behaviour once this story is implemented?

Given <context>
When <event>
Then <should behave like...>

Gerkhin template is described at [Dan North Blog](https://dannorth.net/introducing-bdd/) -->

### UX/UI Details

<!-- Include images, icons and references to external sources like Zeplin -->
