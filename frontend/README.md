# Community Presence Map - Frontend

Community Presence Map frontend is built with [React](https://facebook.github.io/react/).

## Basic structure

| Component | Description                                                                                                                                                                                                                                                                                     |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| App       | Core App                                                                                                                                                                                                                                                                                        |
| Surveys   | Categories of data describing given circumstances, such as "Deprivation", "Risk" or "Destitution". Conceptually, survey indices can be further broken down into domains. For instance, the "Risk" index contains domains such as fires, floods, or loneliness. Indices represent external data. |
| Map       | MapBox components and layers to visualise data.                                                                                                                                                                                                                                                 |
| Resources | Categories of internal BRC resources, such as "Staff & Volunteers" or "Properties".                                                                                                                                                                                                             |
| Shared    | Components and files used by various parts of the app.                                                                                                                                                                                                                                          |

## Configuration

The survey and resource data the app is displaying is defined in `Core/Config/config.json`. Editing this file will result in direct changes to the application interface.

## Development

### Prerequisites

You will need to install:

#### NodeJS

```console
sudo apt install nodejs
node --version
```

#### npm

**Note**: Although both will work, using the alternative package manager `yarn` is strongly encouraged for this project
due our inclusion of a `yarn.lock` file.

```console
sudo apt install npm
check the version:   npm --version
```

#### [Yarn](https://yarnpkg.com/en/)

Clone the repository and install node modules. Run the following

```console
yarn
# or npm install
```

### Environment variables

Secret variables, such as the Mapbox access token, are stored in environment variables. In the CI/CD environment, they will are configured and be will be provided automatically.

For local development, make sure to provide them manually or in a `.env` file in the `frontend` directory. An `.env` file with the development configuration can be found in Teams General files.

### Running

You can run the frontend using `docker-compose`, which will also start the backend `tileserver`. For more on this, see the main project `README.md`.

Alternatively, to just run the React app in development mode, run `yarn start`.

### Testing

To run the tests in watch mode

```console
yarn test
# or npm test
```

To run a particular suite, include a pattern matching the name of that file. For example, to run all of the MapFeatures tests

```console
yarn test MapFeatures
# or npm test MapFeatures
```

### Docker

Note that the development Dockerfile does not copy the app contents into the container. Instead, the `frontend` directory is mounted as a volume, to allow for hot reloading changes. The easiest way to handle this is using `docker-compose up` in the project root directory. To reproduce this using just `docker`, run

```
docker build . --file dev.Dockerfile --tag community-map_frontend
docker run --volume /absolute/path/to/project/community-map/frontend:/app/ --env NODE_ENV=development --publish 3000:3000 community-map_frontend
```

### Code style, linting and formatting

[ESLint](https://github.com/eslint/eslint) and [Prettier](https://github.com/prettier/prettier) are used to ensure a consistent coding style throughout the project. In this setup, Prettier formatting is configured as an ESLint rule. More information on the configuration options etc. in relation to React/TypeScript projects can be found [here](https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project).

It is recommended to configure the respective IDE to use the provided rules. VS Code provides [very good support](https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project). IntelliJ and WebStorm also have [good support](https://prettier.io/docs/en/webstorm.html); they will automatically discover the Prettier settings from `.prettierrc.js` and use them for the "Reformat with Prettier" action (`Alt-Shift-Cmd-P` on macOS). In order to use the prettier config for the regular reformatting shortcut in IntelliJ or WebStorm (`Alt-Cmd-L`), open `.prettierrc.js`, right click and select `Apply Prettier Code Style Rules`.

To run ESLint and automatically fix some issues, run `yarn run lint`. In order to do a dry run and check for any linting issues, you can run the command `yarn run lint:check`.

With the help of `husky` and `lint-staged`, a git pre-commit hook will lint staged files before performing a commit and automatically fix issues where possible or prevent the commit otherwise. If there is a good reason to circumvent this, you can `git commit --no-verify` (or temporarily unselect git commit hooks in your IDE).
