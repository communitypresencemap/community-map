import React from "react"
import { Layer, Source } from "react-mapbox-gl"
import { NumberRange } from "../../Shared/Controls/RangeInput/RangeInput"
import { getLegendValuesFor, LegendValue } from "../../Surveys/SurveyDomainMenu/QuantileLegend/LegendValue"
import { getTilesUrl } from "../../Shared/Utils/fetchData"

interface SurveyLayerProps {
    maxRange: number
    surveyDomain: string
    sourceLayer: string
    fileIdentifier: string
    range: NumberRange
    riskDomains: string[]
    surveyDataType: string
}

const getRange = (start: number, end: number) => {
    const range = []
    for (let i = start; i <= end; i++) {
        range.push(i)
    }
    return range
}

export function SurveyLayer(props: SurveyLayerProps) {
    const { range, sourceLayer, fileIdentifier, surveyDomain, maxRange, riskDomains, surveyDataType } = props

    const vectorSourceOptions = {
        type: "vector",
        tiles: [getTilesUrl(fileIdentifier)],
    }

    const domain = surveyDataType === "Risk" ? riskDomains[0] : surveyDomain
    const usedRange = range ? { start: range.start, end: range.end } : { start: 1, end: maxRange }
    let filter: any = ["in", domain, ...getRange(usedRange.start, usedRange.end)]

    if (riskDomains.length > 1) {
        filter = ["all"]
        riskDomains.forEach(domain => {
            // only show areas where the quantile for the selected risk domains == 1
            filter.push(["<=", domain, 2])
        })
    }

    const stops = () => {
        const transparentStop: [number, string] = [-1, "rgba(0, 0, 0, 0)"]
        const colouredStops: [number, string][] = getLegendValuesFor(maxRange).map((item: LegendValue) => [
            item.value,
            item.color,
        ])
        return [transparentStop, ...colouredStops]
    }

    const sourceId = `source_${fileIdentifier}`

    return (
        <React.Fragment key={fileIdentifier}>
            <Source id={sourceId} tileJsonSource={vectorSourceOptions} />
            <Layer
                onMouseMove={() => {}}
                onMouseLeave={() => {}}
                type="fill"
                id={`layer_${fileIdentifier}`}
                sourceId={sourceId}
                sourceLayer={sourceLayer}
                paint={{
                    "fill-color": {
                        property: domain,
                        stops: stops(),
                    },
                    "fill-opacity": 0.8,
                }}
                filter={filter}
                before={"layer_first"}
            />
        </React.Fragment>
    )
}

export default React.memo(SurveyLayer)
