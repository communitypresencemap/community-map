import { Layer, Source } from "react-mapbox-gl"
import React from "react"
import { getTilesUrl } from "../../Shared/Utils/fetchData"

interface ResourceLayerProps {
    dataLabel: string
    filter: any
    image: HTMLImageElement
}

export const ResourceLayer = (props: ResourceLayerProps) => {
    const { filter, image, dataLabel } = props
    const vectorSourceOptions = {
        type: "vector",
        tiles: [getTilesUrl(`${dataLabel}_uk`)],
    }

    const images: any = [`${dataLabel}_id`, image]
    const layoutLayer = { 
        "icon-image": `${dataLabel}_id`,
        "icon-allow-overlap": true,
    }

    return (
        <>
            <Source id={`source_${dataLabel}`} tileJsonSource={vectorSourceOptions} />
            <Layer
                id={`layer_${dataLabel}`}
                type={"symbol"}
                sourceId={`source_${dataLabel}`}
                sourceLayer={`${dataLabel}`}
                layout={layoutLayer}
                images={images}
                filter={filter}
            />
        </>
    )
}
