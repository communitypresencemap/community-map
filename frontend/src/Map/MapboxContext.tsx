import React, { createContext, useState } from "react"

export const MapboxContext = createContext<any>({})

export function MapboxContextWrapper(props: any) {
    const [map, setMap] = useState<any>({
        flyTo: undefined,
        getZoom: () => 6,
        getCenter: () => [],
    })

    const value = {
        map,
        setMap,
    }
    return <MapboxContext.Provider value={value}>{props.children}</MapboxContext.Provider>
}
