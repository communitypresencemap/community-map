import React, { useContext } from "react"
import { Layer, Source } from "react-mapbox-gl"
import { BoundaryContext, BoundaryWithSource } from "../../Boundaries/BoundaryContext"
import { getTilesUrl } from "../../Shared/Utils/fetchData"

export function BoundaryLayer() {
    const { boundary }: { boundary: BoundaryWithSource } = useContext(BoundaryContext)
    const { fileIdentifier, source } = boundary
    const sourceLayer = source

    const sourceId = `source_${fileIdentifier}`

    const vectorSourceOptions = {
        type: "vector",
        tiles: [getTilesUrl(fileIdentifier)],
    }

    return (
        <React.Fragment key={fileIdentifier}>
            <Source id={sourceId} tileJsonSource={vectorSourceOptions} />
            <Layer
                onMouseMove={() => {}}
                onMouseLeave={() => {}}
                id={`layer_${fileIdentifier}`}
                sourceId={sourceId}
                sourceLayer={sourceLayer}
                type={"line"}
                paint={{
                    "line-color": "rgba(208, 2, 27, 0.75)",
                }}
                before={"layer_second"}
            />
        </React.Fragment>
    )
}
