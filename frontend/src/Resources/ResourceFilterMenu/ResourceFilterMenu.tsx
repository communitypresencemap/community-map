import React, { useContext } from "react"
import { BackButton } from "../../Shared/Controls/BackButton/BackButton"
import { TabButtonMenu } from "../../Shared/Controls/TabButtonMenu/TabButtonMenu"
import { ClearButton } from "../../Shared/Controls/ClearButton/ClearButton"
import { ResourceContext } from "../ResourceContext"
import { CheckButtonList } from "../../Shared/Controls/CheckButtonList/CheckButtonList"
import { extractSelectedOptions } from "../../Shared/Utils/extractFilterOptions"

interface ResourceFilterMenuProps {
    menu: string
    goBack: () => void
}

export function ResourceFilterMenu(props: ResourceFilterMenuProps) {
    const { tab, setTab, resources, setFilter, menuData, tabData, getFilterCount, clearFilter } = useContext(
        ResourceContext,
    )

    return (
        <div className="header-section">
            <div className="filter-settings-header">
                <BackButton onClick={props.goBack} />
                <h2 className="filter-title">{props.menu}</h2>
                <span className="filter-settings-header-buffer" />
            </div>

            <TabButtonMenu
                tabs={menuData.tabs.map((data: any) => ({ label: data.title, value: data.title }))}
                onClick={setTab}
                selectedTab={{ label: tab, value: tab }}
            />
            <div className="body-section">
                {tabData.filters.map((filter: any) => (
                    <CheckButtonList
                        title={filter.label}
                        options={Object.keys(resources[tab][filter.value]).map((data: string) => ({
                            label: data,
                            value: data,
                        }))}
                        onClick={setFilter(filter.value)}
                        selectedValues={extractSelectedOptions(resources[tab][filter.value])}
                        color="red"
                        key={filter.label}
                    />
                ))}
                <hr />

                <ClearButton clearFunctions={[() => clearFilter(tab)]} filterCounts={[getFilterCount(tab)]} />
            </div>
        </div>
    )
}
