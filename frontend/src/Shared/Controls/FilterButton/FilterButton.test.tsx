import React from "react"
import { shallow } from "enzyme"
import { FilterButton } from "./FilterButton"

describe("<FilterButton/>", () => {
    it("should display the label from its option props", () => {
        const expectedLabel = "Services"
        const option = { label: expectedLabel, value: "some-value" }
        const wrapper = shallow(
            <FilterButton option={option} onClick={() => {}}>
                <div />
            </FilterButton>,
        )
        const filterButtonLabel = wrapper.find(".filter-label").text()
        expect(filterButtonLabel).toEqual(expectedLabel)
    })

    it("should call the onClick function from its props when clicked", () => {
        const filterValue = "some-value"
        const option = { label: "Services", value: filterValue }
        const mockOnClick = jest.fn()

        const wrapper = shallow(
            <FilterButton option={option} onClick={mockOnClick}>
                <div />
            </FilterButton>,
        )
        wrapper.simulate("click")

        expect(mockOnClick).toHaveBeenCalledWith(filterValue)
    })
})
