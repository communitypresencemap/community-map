export interface ListButtonOption<T> {
    label: string
    value: T
}
