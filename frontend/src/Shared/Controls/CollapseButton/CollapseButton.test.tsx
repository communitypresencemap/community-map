import { shallow } from "enzyme"
import { CollapseButton } from "./CollapseButton"
import React from "react"

describe("<CollapseButton/>", () => {
    it("should indicate that the pane is closed when closed", () => {
        const wrapper = shallow(<CollapseButton isCollapsed={true} toggleCollapsed={jest.fn()} />)
        expect(wrapper.find(".collapse-button--closed")).toHaveLength(1)
    })

    it("should indicate that the pane is open when open", () => {
        const wrapper = shallow(<CollapseButton isCollapsed={false} toggleCollapsed={jest.fn()} />)
        expect(wrapper.find(".collapse-button--closed")).toHaveLength(0)
    })

    it("should toggle the pane open/closed state when clicked", () => {
        const mockToggleCollapsed = jest.fn()
        const wrapper = shallow(<CollapseButton isCollapsed={false} toggleCollapsed={mockToggleCollapsed} />)
        wrapper.simulate("click")
        expect(mockToggleCollapsed).toHaveBeenCalled()
    })
})
