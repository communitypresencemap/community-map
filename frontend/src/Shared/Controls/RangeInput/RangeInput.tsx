import "./RangeInput.css"

import * as React from "react"
import { useEffect, useState } from "react"

export interface NumberRange {
    start: number
    end: number
}

interface RangeInputProps {
    minValue: number
    maxValue: number
    setRange: (range: NumberRange) => void
    range: NumberRange
}

export const RangeInput = (props: RangeInputProps) => {
    const [start, setStart] = useState<number | string>(props.minValue)
    const [end, setEnd] = useState<number | string>(props.maxValue)

    useEffect(() => {
        if (props.range) {
            setStart(props.range.start)
            setEnd(props.range.end)
        }
    }, [props.range])

    const isInValidRange = (value: number | string): boolean => {
        return value <= props.maxValue && value >= props.minValue
    }

    const handleStart = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = parseInt(event.currentTarget.value, 10) || ""
        setStart(value)

        const valueToValidate = value as number
        if (isInValidRange(valueToValidate) && valueToValidate <= end) {
            props.setRange({ start: valueToValidate, end: end as number })
        }
    }

    const handleEnd = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = parseInt(event.currentTarget.value, 10) || ""
        setEnd(value)

        const valueToValidate = value as number
        if (isInValidRange(valueToValidate) && start <= value) {
            props.setRange({ start: start as number, end: valueToValidate })
        }
    }

    const rangeValid = start <= end
    const validStartClass = isInValidRange(start) && rangeValid ? "" : "invalid"
    const validEndClass = isInValidRange(end) && rangeValid ? "" : "invalid"

    return (
        <div className="range-input-container">
            <input
                className={`range-input ${validStartClass}`}
                value={start}
                onChange={handleStart}
                type="number"
                min={props.minValue}
                max={props.maxValue}
            />
            to
            <input
                className={`range-input ${validEndClass}`}
                value={end}
                onChange={handleEnd}
                type="number"
                min={props.minValue}
                max={props.maxValue}
            />
        </div>
    )
}
