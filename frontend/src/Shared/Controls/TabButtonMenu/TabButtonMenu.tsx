import "./TabButtonMenu.css"

import { TabButton } from "../TabButton/TabButton"
import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface TabButtonMenuProps<T> {
    tabs: ListButtonOption<T>[]
    onClick: (value: T) => void
    selectedTab: ListButtonOption<T>
}

export function TabButtonMenu<T>(props: TabButtonMenuProps<T>) {
    return (
        <div className="tab-menu">
            {props.tabs.map(tab => (
                <TabButton
                    isSelected={props.selectedTab.value === tab.value}
                    onClick={props.onClick}
                    option={tab}
                    key={tab.label}
                />
            ))}
        </div>
    )
}
