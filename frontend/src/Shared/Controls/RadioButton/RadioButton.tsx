import "./RadioButton.css"
import "../Controls.css"

import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface RadioButtonProps<T> {
    option: ListButtonOption<T>
    isSelected: boolean
    onClick: (value: T) => void
    color: string
}

export function RadioButton<T>(props: RadioButtonProps<T>) {
    const { onClick, option, color, isSelected } = props

    const className = isSelected ? "selected" : ""
    return (
        <div onClick={() => onClick(option.value)} className="rb">
            <span className={`rb-outer ${className} ${color}`}>
                <span className={`rb-inner ${className} ${color}`} />
            </span>
            <span className="rb-text">{option.label}</span>
        </div>
    )
}
