import React from "react"
import { mount, shallow } from "enzyme"
import { CheckButton } from "../CheckButton/CheckButton"
import { CheckButtonList } from "./CheckButtonList"

describe("<CheckButtonList/>", () => {
    const options = ["UK Office", "Shared Service Centre", "Red Cross Training"].map(option => {
        return { label: option, value: option }
    })

    it("should display the given title", () => {
        const expectedTitle = "some-title"
        const wrapper = mount(
            <CheckButtonList
                title={expectedTitle}
                options={options}
                onClick={() => {}}
                selectedValues={[]}
                color="red"
            />,
        )
        const CheckButtonListTitle = wrapper.find(".check-button-title").text()
        expect(CheckButtonListTitle).toEqual(expectedTitle)
    })

    it("should contain the correct amount of CheckButtonList for the given options", () => {
        const wrapper = shallow(
            <CheckButtonList title="some-title" options={options} onClick={() => {}} selectedValues={[]} color="red" />,
        )
        expect(wrapper.find(CheckButton).length).toEqual(options.length)
    })

    it("should have the correct label on the check button for each option", () => {
        const wrapper = mount(
            <CheckButtonList title="some-title" options={options} onClick={() => {}} selectedValues={[]} color="red" />,
        )
        const checkButtons = wrapper.find(CheckButton)
        checkButtons.forEach((checkButton, i) => {
            const checkButtonLabel = checkButton.find(".check-button-label").text()
            expect(checkButtonLabel).toEqual(options[i].label)
        })
    })

    it("should call the setter with the correct value for each option", () => {
        const mockOnClick = jest.fn()
        const wrapper = mount(
            <CheckButtonList
                title="some-title"
                options={options}
                onClick={mockOnClick}
                selectedValues={[]}
                color="red"
            />,
        )
        const checkButtons = wrapper.find(CheckButton)
        checkButtons.forEach((checkButton, i) => {
            checkButton.simulate("click")
            expect(mockOnClick).toHaveBeenCalledWith(options[i].value)
        })
    })

    it("should select the correct check buttons based on the props", () => {
        const selectedValues = ["UK Office", "Shared Service Centre"]
        const wrapper = mount(
            <CheckButtonList
                title="some-title"
                options={options}
                onClick={() => {}}
                selectedValues={selectedValues}
                color="red"
            />,
        )
        const checkButtons = wrapper.find(CheckButton)
        checkButtons.forEach((checkButton, i) => {
            const expectedIsSelected = selectedValues.includes(options[i].value)
            const checkButtonIsSelected = checkButton.find(".check-box").hasClass("check-box--selected")
            expect(checkButtonIsSelected).toEqual(expectedIsSelected)
        })
    })
})
