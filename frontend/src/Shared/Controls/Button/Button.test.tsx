import { mount } from "enzyme"
import * as React from "react"
import { Button } from "./Button"

describe("<Button>", () => {
    it("should call function onClick", () => {
        const mock = jest.fn()
        const wrapper = mount(<Button onClick={mock} label={""} />)

        wrapper.find(Button).simulate("click")

        expect(mock).toHaveBeenCalledTimes(1)
    })

    it("should render label", () => {
        const label = "Click me"
        const wrapper = mount(<Button onClick={jest.fn()} label={label} />)

        expect(wrapper.find(Button).text()).toEqual(label)
    })
})
