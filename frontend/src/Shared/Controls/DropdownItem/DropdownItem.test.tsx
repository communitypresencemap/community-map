import { mount } from "enzyme"
import * as React from "react"
import { DropdownItem } from "./DropdownItem"

describe("<DropdownItem>", () => {
    it("should call function onClick", () => {
        const mock = jest.fn()
        const wrapper = mount(<DropdownItem onClick={mock} label={""} />)

        wrapper.find(DropdownItem).simulate("click")

        expect(mock).toHaveBeenCalledTimes(1)
    })

    it("should render label", () => {
        const label = "Click me"
        const wrapper = mount(<DropdownItem onClick={jest.fn()} label={label} />)

        expect(wrapper.find(DropdownItem).text()).toEqual(label)
    })

    it("should not call function onClick if the button is disabled", () => {
        const mock = jest.fn()
        const wrapper = mount(<DropdownItem onClick={mock} label={""} disabled={true} />)

        const element = wrapper.find(DropdownItem)
        element.simulate("click")

        expect(mock).toHaveBeenCalledTimes(0)
    })
})
