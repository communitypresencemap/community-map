import { shallow } from "enzyme"
import React from "react"
import { BackButton } from "./BackButton"

describe("<BackButton/>", () => {
    it("should call onClick when clicked", () => {
        const onClick = jest.fn()
        const wrapper = shallow(<BackButton onClick={onClick} />)

        wrapper.simulate("click")

        expect(onClick).toHaveBeenCalled()
    })
})
