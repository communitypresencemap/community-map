export enum QuantileValue {
    Quintile = 5,
    Decile = 10,
}
