import { QuantileValue } from "./QuantileValue"

export const colourValues = {
    [QuantileValue.Quintile]: [
        "rgba(10, 70, 207, 0.8)",
        "rgba(44, 100, 218, 0.8)",
        "rgba(86, 137, 231, 0.8)",
        "rgba(128, 173, 224, 0.8)",
        "rgba(162, 203, 255, 0.8)",
    ],
    [QuantileValue.Decile]: [
        "rgba(10, 70, 207, 0.8)",
        "rgba(27, 85, 212, 0.8)",
        "rgba(44, 100, 218, 0.8)",
        "rgba(61, 114, 223, 0.8)",
        "rgba(78, 129, 228, 0.8)",
        "rgba(94, 144, 234, 0.8)",
        "rgba(111, 159, 239, 0.8)",
        "rgba(128, 173, 224, 0.8)",
        "rgba(145, 188, 250, 0.8)",
        "rgba(162, 203, 255, 0.8)",
    ],
}
