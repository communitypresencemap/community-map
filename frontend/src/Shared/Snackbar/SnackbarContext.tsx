import React, { createContext, useState } from "react"

export const SnackbarContext = createContext<any>({})

export function SnackbarContextWrapper(props: any) {
    const [message, setMessage] = useState<string>("")
    const [isVisible, setIsVisible] = useState<boolean>(false)

    const displayMessage = (message: string, durationInSeconds = 3) => {
        setIsVisible(true)
        setMessage(message)
        setTimeout(() => setIsVisible(false), durationInSeconds * 1000)
    }

    const value = {
        displayMessage,
        message,
        isVisible,
    }

    return <SnackbarContext.Provider value={value}>{props.children}</SnackbarContext.Provider>
}
