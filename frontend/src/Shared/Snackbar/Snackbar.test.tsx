import React from "react"
import { mount } from "enzyme"
import { Snackbar } from "./Snackbar"
import { SnackbarContext } from "./SnackbarContext"

const mountWithSnackbarContext = (message: string, isVisible: boolean) => {
    return mount(
        <SnackbarContext.Provider value={{ message, isVisible }}>
            <Snackbar />
        </SnackbarContext.Provider>,
    )
}

describe("<Snackbar>", function() {
    it("should display the given message", function() {
        const message = "foo"
        const wrapper = mountWithSnackbarContext(message, true)
        expect(wrapper.find("#snackbar").text()).toEqual(message)
    })

    it("should display when isVisible is true", function() {
        const wrapper = mountWithSnackbarContext("", true)
        expect(wrapper.find(".show")).toHaveLength(1)
    })

    it("should not display when isVisible is false", function() {
        const wrapper = mountWithSnackbarContext("", false)
        expect(wrapper.find(".show")).toHaveLength(0)
    })
})
