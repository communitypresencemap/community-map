import { FilterOptions } from "../FilterOptions/FilterOptions"
import { Dispatch, SetStateAction, useState } from "react"

export function useToggleableOption(
    initialState: FilterOptions = {},
): [FilterOptions, (b: string) => void, () => void, Dispatch<SetStateAction<Record<string, boolean>>>] {
    const [state, setState] = useState<FilterOptions>(initialState)
    const toggle = (b: string) => {
        setState({ ...state, [b]: !state[b] })
    }

    const clear: () => void = () => setState({})

    return [state, toggle, clear, setState]
}
