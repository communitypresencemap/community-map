import React, { ReactElement } from "react"
import { mount } from "enzyme"
import { SurveyContextWrapper } from "../../Surveys/SurveyContext"
import { MapboxContextWrapper } from "../../Map/MapboxContext"
import { ResourceContextWrapper } from "../../Resources/ResourceContext"
import { SnackbarContextWrapper } from "../Snackbar/SnackbarContext"

export const mountWithContext = (element: ReactElement) => {
    return mount(
        <SurveyContextWrapper>
            <MapboxContextWrapper>
                <ResourceContextWrapper>
                    <SnackbarContextWrapper>{element}</SnackbarContextWrapper>
                </ResourceContextWrapper>
            </MapboxContextWrapper>
        </SurveyContextWrapper>,
    )
}
