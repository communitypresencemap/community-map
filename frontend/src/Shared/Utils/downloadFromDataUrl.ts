export const downloadFromDataUrl = (dataUrl: string, fileName: string) => {
    const link = document.createElement("a")
    if (link.download !== undefined) {
        // feature detection
        // Browsers that support HTML5 download attribute
        link.setAttribute("href", dataUrl)
        link.setAttribute("download", fileName)
        link.style.visibility = "hidden"
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    }
}
