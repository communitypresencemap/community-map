import "./Pane.css"

import React, { useState } from "react"
import { CollapseButton } from "../Controls/CollapseButton/CollapseButton"
import { ResourcePane } from "../../Resources/ResourcePane/ResourcePane"

export function Pane() {
    const [isCollapsed, setIsCollapsed] = useState(false)
    const paneContainerClass: string = isCollapsed ? "pane-container--closed" : ""
    return (
        <div id="pane-container" className={paneContainerClass}>
            <ResourcePane />
            <CollapseButton isCollapsed={isCollapsed} toggleCollapsed={() => setIsCollapsed(!isCollapsed)} />
        </div>
    )
}
