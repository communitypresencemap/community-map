import React, { useContext } from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import jsPDF from "jspdf"

export const DownloadPdfDropdownItem = () => {
    const { map } = useContext(MapboxContext)

    const handleClick = () => {
        downloadPdf(map.getCanvas(), "map.pdf")
    }

    return <DropdownItem onClick={handleClick} label={"Download PDF"} disabled={map.getCanvas === undefined} />
}

const downloadPdf = (canvas: HTMLCanvasElement, fileName: string) => {
    const { width, height } = canvas
    const pdf = new jsPDF({
        orientation: width > height ? "landscape" : "portrait",
        unit: "px",
        format: [width, height],
        compress: true,
    })
    pdf.addImage(canvas.toDataURL("image/png"), "PNG", 0, 0, width, height, null, "FAST")

    const title = `Community Presence Map Export from ${new Date().toLocaleDateString()}`
    pdf.setProperties({ title })

    pdf.save(fileName)
}
