import { mountWithContext } from "../../Shared/Utils/testUtils"
import { DropdownMenu } from "./DropdownMenu"
import React from "react"
import { Button } from "../../Shared/Controls/Button/Button"

// Mock jsPDF to suppress "Error: Not implemented: HTMLCanvasElement.prototype.getContext"
jest.mock("jspdf", () => {})

describe("<DropdownMenu>", () => {
    it("should not reveal menu options initially", () => {
        const wrapper = mountWithContext(
            <DropdownMenu label={"anystring"}>
                <div></div>
            </DropdownMenu>,
        )
        expect(wrapper.find(".dropdown-items")).toHaveLength(0)
    })

    it("should reveal menu options on clicking the button", () => {
        const wrapper = mountWithContext(
            <DropdownMenu label={"anystring"}>
                <div></div>
            </DropdownMenu>,
        )
        wrapper.find(Button).simulate("click")
        expect(wrapper.find(".dropdown-items")).toHaveLength(1)
    })
})
