import "./DropdownMenu.css"
import React, { useState } from "react"
import { Button } from "../../Shared/Controls/Button/Button"

export interface DropdownMenuProps {
    label: string
    children: any
}

export const DropdownMenu = (props: DropdownMenuProps) => {
    const [collapsed, setCollapsed] = useState<boolean>(true)

    return (
        <div className="dropdown-menu">
            <Button onClick={() => setCollapsed(!collapsed)} label={props.label} />
            {!collapsed && <div className={"dropdown-items"}>{props.children}</div>}
        </div>
    )
}
