import React, { useContext } from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { SurveyContext } from "../../Surveys/SurveyContext"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { SnackbarContext } from "../../Shared/Snackbar/SnackbarContext"
import { ResourceContext } from "../../Resources/ResourceContext"
import { BoundaryContext } from "../../Boundaries/BoundaryContext"

export function ShareDropdownItem() {
    const { map } = useContext(MapboxContext)
    const { survey } = useContext(SurveyContext)
    const { displayMessage } = useContext(SnackbarContext)
    const { resources } = useContext(ResourceContext)
    const { boundary } = useContext(BoundaryContext)

    const handleClick = () => {
        const currentState = {
            map: { zoom: map.getZoom(), center: map.getCenter() },
            survey,
            resources,
            boundary,
        }

        const state64 = btoa(JSON.stringify(currentState))
        const shareLink = `${window.location.origin}/state?${state64}`

        const temp = document.createElement("textarea")
        temp.value = shareLink
        document.body.appendChild(temp)
        temp.select()
        document.execCommand("copy")
        document.body.removeChild(temp)

        displayMessage("Copied to clipboard...")
    }

    return (
        <>
            <DropdownItem onClick={handleClick} label={"Copy link to share view"} />
        </>
    )
}
