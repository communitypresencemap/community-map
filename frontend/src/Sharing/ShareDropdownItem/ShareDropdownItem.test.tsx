import React from "react"
import { ShareDropdownItem } from "./ShareDropdownItem"
import { mountWithContext } from "../../Shared/Utils/testUtils"

describe("<ShareDropdownItem>", () => {
    it("should not crash when no survey data is selected", () => {
        document.execCommand = () => false

        const wrapper = mountWithContext(<ShareDropdownItem />)

        wrapper.find("button").simulate("click")
    })
})
