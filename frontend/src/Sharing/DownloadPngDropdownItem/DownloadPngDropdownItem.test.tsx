import { mountWithContext } from "../../Shared/Utils/testUtils"
import React, { ReactElement } from "react"
import { DownloadPngDropdownItem } from "./DownloadPngDropdownItem"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { MapboxContext } from "../../Map/MapboxContext"

function mountWithContextAndMockMap(element: ReactElement, expectedDataUrl = "") {
    const MockMap = {
        getCanvas: () => ({
            toDataURL: () => expectedDataUrl,
        }),
    }
    return mountWithContext(<MapboxContext.Provider value={{ map: MockMap }}>{element}</MapboxContext.Provider>)
}

describe("<DownloadPngDropdownItem>", function() {
    it("should be disabled while the Map component is not ready", function() {
        const wrapper = mountWithContext(<DownloadPngDropdownItem />)
        expect(wrapper.find(DropdownItem).props().disabled).toEqual(true)
    })

    it("should be enabled once the Map component is ready", function() {
        const wrapper = mountWithContextAndMockMap(<DownloadPngDropdownItem />)
        expect(wrapper.find(DropdownItem).props().disabled).toEqual(false)
    })

    it("should download image when being clicked on", () => {
        const expectedFileName = "map.png"
        const expectedDataUrl = "data:image/png;base64,foobar"

        const wrapper = mountWithContextAndMockMap(<DownloadPngDropdownItem />, expectedDataUrl)

        const link = document.createElement("a")
        link.click = jest.fn()
        jest.spyOn(document, "createElement").mockImplementation((_: string) => link)
        wrapper.find("button").simulate("click")

        expect(link.download).toEqual(expectedFileName)
        expect(link.href).toEqual(expectedDataUrl)
        expect(link.click).toHaveBeenCalledTimes(1)
    })
})
