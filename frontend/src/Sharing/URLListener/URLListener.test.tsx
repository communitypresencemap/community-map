import { mountWithContext } from "../../Shared/Utils/testUtils"
import React from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { URLListener } from "./URLListener"

describe("<URLListener>", () => {
    it("should call map.flyTo if the map is loaded", () => {
        const mockFlyTo = jest.fn()

        const mockMapContext = {
            setMap: jest.fn(),
            map: {
                flyTo: mockFlyTo,
                getZoom: true,
                getCenter: true,
            },
        }

        mountWithContext(
            <MapboxContext.Provider value={mockMapContext}>
                <URLListener />
            </MapboxContext.Provider>,
        )

        expect(mockFlyTo).toHaveBeenCalledTimes(1)
    })

    it("should not call map.flyTo if map is not loaded", () => {
        const mockFlyTo = jest.fn()

        const mockMapContext = {
            setMap: jest.fn(),
            map: {
                flyTo: mockFlyTo,
            },
        }

        mountWithContext(
            <MapboxContext.Provider value={mockMapContext}>
                <URLListener />
            </MapboxContext.Provider>,
        )

        expect(mockFlyTo).toHaveBeenCalledTimes(0)
    })
})
