import "./BoundaryMenu.css"
import React, { useContext } from "react"
import { DropdownMenu } from "../../Sharing/DropdownMenu/DropdownMenu"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { BoundaryContext } from "../BoundaryContext"
import config from "../../Core/Config/config"

export const BoundaryMenu = () => {
    const { boundary: currentBoundary, setBoundary } = useContext(BoundaryContext)
    return (
        <div id="boundary-menu">
            <DropdownMenu label={"Locations"}>
                {config.data.boundaries.map(boundary => (
                    <DropdownItem
                        onClick={() => setBoundary(boundary.label)}
                        label={boundary.label}
                        key={boundary.label}
                        selected={currentBoundary.fileIdentifier === boundary.fileIdentifier}
                    />
                ))}
            </DropdownMenu>
        </div>
    )
}
