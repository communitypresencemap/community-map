import { getLegendValuesFor } from "./LegendValue"

describe("LegendValue", function() {
    const legendItems = getLegendValuesFor(10)
    it("should get an array of 10 items", function() {
        expect(legendItems).toHaveLength(10)
    })
    it("should have the min color in the first element", function() {
        expect(legendItems[0].color).toEqual("#67000d")
    })
})
