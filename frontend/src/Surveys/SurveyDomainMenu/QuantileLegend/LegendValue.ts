export interface LegendValue {
    value: number
    color: string
}

export const getLegendValuesFor = (maxRange: number) => {
    const max = { r: 0xff, g: 0xf5, b: 0xf0 }  // original blue: { r: 0xa2, g: 0xcb, b: 0xff }
    const min = { r: 0x67, g: 0x00, b: 0x0d }  // original blue: { r: 0x0a, g: 0x46, b: 0xcf }
    const inc = {
        r: (max.r - min.r) / (maxRange - 1),
        g: (max.g - min.g) / (maxRange - 1),
        b: (max.b - min.b) / (maxRange - 1),
    }

    const fromRGB = (rgb: Record<string, number>) => (rgb.r << 16) + (rgb.g << 8) + rgb.b

    const legendColors = []
    for (let i = fromRGB(min); i <= fromRGB(max); i += fromRGB(inc)) {
        const color = Math.floor(i).toString(16)
        const paddedColor = color.length === 5 ? `#0${color}` : `#${color}`
        legendColors.push(paddedColor)
    }

    return legendColors.map((color, i) => ({ value: i + 1, color }))
}
