import "./LegendItem.css"

import React, { useContext } from "react"
import { LegendValue } from "./LegendValue"
import { SurveyContext } from "../../SurveyContext"
import { extractSelectedOptions } from "../../../Shared/Utils/extractFilterOptions"

interface LegendItemProps {
    legendItem: LegendValue
}

export function LegendItem({ legendItem }: LegendItemProps) {
    const { range, riskDomains, maxRange } = useContext(SurveyContext)

    const { value } = legendItem
    let { color } = legendItem

    const riskSelected = extractSelectedOptions(riskDomains).length > 1 && value !== 1 && value !== 2  //maxRange
    if ((range && (value > range.end || value < range.start)) || riskSelected) {
        color = "rgba(235, 235, 235, 0.8)"  // colour for the faded quantiles when user selects more than one risk domain
    }

    return (
        <div className="legend-item">
            <span className="legend-key" style={{ backgroundColor: color }} />
            <span>{value}</span>
        </div>
    )
}
