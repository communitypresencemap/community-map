import React, { useContext } from "react"
import { RadioButtonList } from "../../../Shared/Controls/RadioButtonList/RadioButtonList"
import { RangeInput } from "../../../Shared/Controls/RangeInput/RangeInput"
import { SurveyContext } from "../../SurveyContext"
import { CheckButtonList } from "../../../Shared/Controls/CheckButtonList/CheckButtonList"
import { extractSelectedOptions } from "../../../Shared/Utils/extractFilterOptions"

interface DomainSettingsBodyProps {
    getCollapseProps: any
}

export function DomainSettingsBody(props: DomainSettingsBodyProps) {
    const {
        range,
        setRange,
        filters,
        riskDomains,
        toggleRiskDomains,
        dataType,
        domain,
        setSurveyDomain,
        maxRange,
        sources,
        setSurveySource,
        selectedSource,
    } = useContext(SurveyContext)
    const title = `${dataType} type`
    return (
        <div id="domain-settings-body" {...props.getCollapseProps()}>
            {extractSelectedOptions(riskDomains).length <= 1 && (
                <RangeInput minValue={1} maxValue={maxRange} setRange={setRange} range={range} />
            )}

            {sources.length > 1 && (
                <RadioButtonList
                    title="Granularity"
                    options={sources}
                    selectedValue={selectedSource}
                    setSelectedOption={setSurveySource}
                    color="teal"
                />
            )}

            <div className="body-section">
                {dataType === "Risk" ? (
                    <CheckButtonList
                        title={title}
                        options={filters}
                        onClick={toggleRiskDomains}
                        selectedValues={extractSelectedOptions(riskDomains)}
                        color="teal"
                    />
                ) : (
                    <RadioButtonList
                        title={title}
                        options={filters}
                        selectedValue={domain}
                        setSelectedOption={setSurveyDomain}
                        color="teal"
                    />
                )}
            </div>
        </div>
    )
}
