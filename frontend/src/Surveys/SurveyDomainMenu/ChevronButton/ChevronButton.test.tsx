import { shallow } from "enzyme"
import React from "react"
import { ChevronButton } from "./ChevronButton"

describe("<ChevronButton/>", () => {
    it("should point up when isOpen is false", () => {
        const wrapper = shallow(<ChevronButton isOpen={false} onClick={jest.fn()} />)
        const button = wrapper.find(".chevron-button")
        expect(button.hasClass("chevron-button--closed")).toBe(true)
    })

    it("should point down when isOpen is true", () => {
        const wrapper = shallow(<ChevronButton isOpen={true} onClick={jest.fn()} />)
        const button = wrapper.find(".chevron-button")
        expect(button.hasClass("chevron-button--closed")).toBe(false)
    })

    it("should call the onClick prop when clicked", () => {
        const mockOnClick = jest.fn()

        const wrapper = shallow(<ChevronButton isOpen={false} onClick={mockOnClick} />)
        wrapper.simulate("click")

        expect(mockOnClick).toHaveBeenCalled()
    })
})
