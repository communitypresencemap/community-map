import React from "react"
import { shallow } from "enzyme"
import { MenuButton } from "./MenuButton/MenuButton"
import { SurveyDataTypeMenu } from "./SurveyDataTypeMenu"
import config from "../../Core/Config/config"

function shallowSurveyDataTypeMenu() {
    return shallow(<SurveyDataTypeMenu />)
}

describe("<SurveyDataTypeMenu/>", () => {
    const SurveyDataTypes = config.data.surveys.map(survey => survey.title)
    it("should have an <MenuButton/> for every survey datatype", () => {
        const numberOfSurveyDataTypes = SurveyDataTypes.length
        const wrapper = shallowSurveyDataTypeMenu()
        const externalButtons = wrapper.find(MenuButton)
        expect(externalButtons).toHaveLength(numberOfSurveyDataTypes)
    })

    it("should display the name of the survey datatype in the button", () => {
        const numberOfSurveyDataTypes = SurveyDataTypes.length

        const wrapper = shallowSurveyDataTypeMenu()
        const buttons = wrapper.find(MenuButton)

        for (let i = 0; i < numberOfSurveyDataTypes; i++) {
            const expected = SurveyDataTypes[i]
            const buttonText = buttons.get(i).props.text
            expect(buttonText).toEqual(expected)
        }
    })
})
