import "./MenuButton.css"

import React from "react"

interface MenuButtonProps {
    text: string
    isSelected: boolean
    onClick: (value: string) => void
    buttonId: string
}

export const MenuButton = (props: MenuButtonProps) => {
    const { onClick, isSelected, text, buttonId } = props

    const className = `menu-button ${isSelected ? "selected" : ""}`
    return (
        <div id={buttonId} onClick={() => onClick(text)} className={className}>
            {text}
        </div>
    )
}
