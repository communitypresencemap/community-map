import React from "react"
import App from "./App"
import { mount } from "enzyme"

jest.mock("react-mapbox-gl", () => ({
    __esModule: true,
    // return function that returns null to represent rendering nothing
    default: () => () => null,
    Layer: () => {},
}))

// Mock jsPDF to suppress "Error: Not implemented: HTMLCanvasElement.prototype.getContext"
jest.mock("jspdf", () => {})

describe("<App/>", () => {
    it("renders without crashing", () => {
        mount(<App />)
    })
})
