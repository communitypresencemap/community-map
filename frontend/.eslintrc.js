module.exports = {
    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
    extends: [
        "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
        "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
        "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
        "plugin:prettier/recommended", // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    parserOptions: {
        ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
        sourceType: "module", // Allows for the use of imports
        ecmaFeatures: {
            jsx: true, // Allows for the parsing of JSX
        },
    },
    rules: {
        // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
        "@typescript-eslint/no-use-before-define": "off",
        // This might be turned on, but turning it off reflects the current state of the code
        "@typescript-eslint/explicit-function-return-type": "off",
        // This might be turned on, but turning it off reflects the current state of the code
        "@typescript-eslint/no-explicit-any": "off",
        // Component display names are good for debugging but not always needed
        "react/display-name": "off",
        // Disable the base rule (as opposed to TS specific rule) as it can report incorrect errors
        "no-unused-vars": "off",
        // Allow certain naming patterns for unused args
        "@typescript-eslint/no-unused-vars": ["warn", {
            "vars": "all",
            "varsIgnorePattern": "^_",
            "argsIgnorePattern": "^_",
            "args": "after-used",
            "ignoreRestSiblings": false
        }],
        "@typescript-eslint/no-var-requires": "off"
    },
    settings: {
        react: {
            version: "detect", // Tells eslint-plugin-react to automatically detect the version of React to use
        },
    },
}
