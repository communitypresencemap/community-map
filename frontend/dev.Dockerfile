FROM node:12.2.0-alpine

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn

EXPOSE 3000

CMD ["yarn", "start"]
